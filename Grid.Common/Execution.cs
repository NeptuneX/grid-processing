﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Grid.Common
{
    public static class Execution
    {
        /// <summary>
        /// Executes an action based on the condition specified
        /// </summary>
        /// <param name="condition">condition to evaluate</param>
        /// <param name="actionIfTrue">action to be executed if the condition is true</param>
        /// <param name="actionifFalse">action to be executed if the condition is false</param>
        [System.Diagnostics.DebuggerStepThrough()]
        public static void If(Func<bool> condition, Action actionIfTrue, Action actionifFalse)
        {
            Contract.Requires(condition != null);
            Contract.Requires(actionifFalse != null);
            Contract.Requires(actionIfTrue != null);

            if (condition())
                actionIfTrue();
            else
                actionifFalse();
        }
        //public static T ReturnIf<T>(Func<bool> condition, Func<T> functionIfTrue, Action<T> functionIfFalse)
        //{
        //    return condition()? functionIfTrue(): functionIfTrue();
        //}
        [System.Diagnostics.DebuggerStepThrough()]
        public static void If<T>(Func<T, bool> condition, T arg, Action<T> actionIfTrue, Action<T> actionifFalse)
        {
            Contract.Requires(condition != null);
            Contract.Requires(actionifFalse != null);
            Contract.Requires(actionIfTrue != null);

            if (condition(arg))
                actionIfTrue(arg);
            else
                actionifFalse(arg);
        }
    }
}
