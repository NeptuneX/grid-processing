using System;
using System.Collections.Generic;
using System.IO;

namespace Grid.Common.Storage
{
    public interface IHolderStorageProvider: IDisposable
    {
        void Initialize(FileInfo file);

        //void SaveToInbox(Object msg);
        void SaveToInbox(IEnumerable<Object> objects);
        //void SaveToOutbox(Object msg);
        void SaveToOutbox(IEnumerable<Object> objects);

        int InboxCount { get; }
        int OutboxCount { get; }


        Object[] GetInboxMessage(int count);
        Object[] GetOutboxMessage(int count);
        void DeleteMessage(Object msg);

        bool IsInboxEmpty { get; }
        bool IsOutboxEmpty { get; }

        void PurgeInbox();
        void PurgeOutbox();
        void PurgeTrash();
        void Clear();
    }
    
}
