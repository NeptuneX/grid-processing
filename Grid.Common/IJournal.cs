﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Common
{
    public interface IJournal
    {
        void Info (string msg, params object[] args);
        void Warn (string msg, params object[] args);
        void Error(string msg, params object[] args);
        void Debug(string msg, params object[] args);

        void Warn (Exception ex, string msg="", params object[] args);
        void Error(Exception ex, string msg="", params object[] args);
        void Debug(Exception ex, string msg="", params object[] args);
    }
}
