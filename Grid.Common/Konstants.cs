﻿using static System.String;

namespace Grid.Common
{
    public static class Konstants
    {
        public const string LocalHost = @"tcp://127.0.0.1";
        public const int ServerNotificationPort = 5555;
        public const string ServerRequestPort = @"5556";
        public static string Url => $"{LocalHost}:{ServerNotificationPort}";
        public const string Topic = @"GridNotification";

    }
}
