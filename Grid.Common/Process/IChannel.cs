﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Common.Process
{
    public interface IChannel:IChannelReader, IChannelWriter
    {
        IEnumerable<T> Read<T>(int count) where T:class, new();
        bool Write<T>(IEnumerable<T> obj, out int remaining) where T:class, new();
    }
}
