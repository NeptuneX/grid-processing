﻿using System.Diagnostics.Contracts;

namespace Grid.Common.Process
{
    public interface IChannelBase
    {
        int Count
        {
            get;
        }

        bool IsEmpty
        {
            get;
        }

        bool IsFull
        {
            get;
        }

        bool IsClosed
        {
            get;
        }

        int Capacity
        {
            get;
        }
    }
}