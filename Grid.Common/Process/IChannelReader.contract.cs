using System;
using System.Diagnostics.Contracts;
namespace Grid.Common.Process
{
    [ContractClassFor(typeof(IChannelReader))]
    internal abstract class IChannelReader_Contract : IChannelReader
    {
        [Pure]
        public int Capacity => default(int);

        [Pure]
        public int Count => default(int);

        [Pure]
        public bool IsClosed => default(bool);

        [Pure]
        public bool IsEmpty => default(bool);

        [Pure]
        public bool IsFull => default(bool);

        public T Read<T>()
        {
            Contract.Ensures(Contract.Result<T>() != null);

            return default(T);
        }
    }
}