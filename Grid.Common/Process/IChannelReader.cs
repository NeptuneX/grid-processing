﻿using System.Diagnostics.Contracts;

namespace Grid.Common.Process
{
    [ContractClass(typeof (IChannelReader_Contract))]
    public interface IChannelReader : IChannelBase
    {
        T Read<T>();
    }
}