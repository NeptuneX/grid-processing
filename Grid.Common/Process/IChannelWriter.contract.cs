using System;
using System.Diagnostics.Contracts;
namespace Grid.Common.Process
{
    [ContractClassFor(typeof(IChannelWriter))]
    internal abstract class IChannelWriter_Contract : IChannelWriter
    {
        [Pure]
        public int Capacity => default(int);

        [Pure]
        public int Count => default(int);

        [Pure]
        public bool IsClosed => default(bool);

        [Pure]
        public bool IsEmpty => default(bool);

        [Pure]
        public bool IsFull => default(bool);

        public void Close()
        {
            Contract.Ensures(IsClosed);
        }

        public void Write<T>(T arg)
        {
            Contract.Requires(arg != null);
            Contract.Requires(!IsClosed);
        }
    }
}