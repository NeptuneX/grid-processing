﻿using System.Diagnostics.Contracts;

namespace Grid.Common.Process
{
    [ContractClass(typeof (IChannelWriter_Contract))]
    public interface IChannelWriter : IChannelBase
    {
        void Write<T>(T arg);
        void Close();
    }
}