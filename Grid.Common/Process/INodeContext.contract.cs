using System;
using System.Diagnostics.Contracts;
namespace Grid.Common.Process
{
    [ContractClassFor(typeof(INodeContext))]
    internal abstract class INodeContext_Contract : INodeContext
    {
        [Pure]
        public IChannelReader In => default(IChannelReader);

        public IChannelReader[] InPipes
        {
            get
            {
                Contract.Ensures(Contract.Result<IChannelReader[]>().Length >= 1);
                return default(IChannelReader[]);
            }
        }

        [Pure]
        public IJournal Journal => default(IJournal);
        [Pure]
        public IChannelWriter Out => default(IChannelWriter);

        public IChannelWriter[] OutPipes
        {
            get
            {
                Contract.Ensures(Contract.Result<IChannelWriter[]>().Length >= 1);
                return default(IChannelWriter[]);
            }
        }

        public void Continue()
        {
        }
        public void Exit()
        {
        }

        public T Read<T>()
        {
            Contract.Requires(In != null);
            Contract.Ensures(Contract.Result<T>()!=null);
            Contract.Ensures(!Contract.Result<T>().Equals(default(T)));

            return default(T);
        }

        public void Repeat()
        {
        }
        public void Restart()
        {
        }

        public void Write<T>(T arg)
        {
            Contract.Requires(Out != null);
            Contract.Requires(arg != null);
            Contract.Requires(!arg.Equals(default(T)));

        }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(In != null);
            Contract.Invariant(Out != null);
            Contract.Invariant(Journal != null);
        }
    }
}