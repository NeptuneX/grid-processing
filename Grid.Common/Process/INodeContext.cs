﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Grid.Common.Process
{
    [ContractClass(typeof (INodeContext_Contract))]
    public interface INodeContext
    {
        IChannelReader In
        {
            get;
        }

        IChannelWriter Out
        {
            get;
        }

        IJournal Journal
        {
            get;
        }

        IChannelReader[] InPipes
        {
            get;
        }

        IChannelWriter[] OutPipes
        {
            get;
        }

        void Repeat();
        void Exit();
        void Restart();
        void Continue();

        T Read<T>();
        void Write<T>(T arg);
    }
}