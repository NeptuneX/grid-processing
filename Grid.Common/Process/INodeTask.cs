﻿using Grid.Common.Process;

namespace Grid.Common.Process
{
    public interface INodeTask
    {
        void Init(INodeContext ctx);
        void Run(INodeContext ctx);
        void Destroy(INodeContext ctx);
    }
}