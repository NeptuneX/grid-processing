﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Common.Process
{
    public abstract class NodeTask : INodeTask
    {
        protected INodeContext Context { get; private set; }

        void INodeTask.Init(INodeContext ctx)
        {
            Context = ctx;
            Init();
        }
        void INodeTask.Destroy(INodeContext ctx)
        {
            Context = ctx;
            Destroy();
        }
        void INodeTask.Run(INodeContext ctx)
        {
            Context = ctx;
            Run();
        }

        protected virtual void Init()
        {
        }
        protected virtual void Destroy()
        {
        }

        protected abstract void Run();
    }
}
