﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Grid.Common
{
    public sealed class Sandbox
    {
        [DebuggerStepThrough()]
        public static bool Try(Action action, Action<Exception> handler)
        {
            Contract.Requires(action != null);
            Contract.Requires(handler != null);

            var ret = true;

            try
            {
                action();
            }
            catch (Exception ex)
            {
                ret = false;
                handler(ex);
            }

            return ret;
        }
        [DebuggerStepThrough()]
        public static bool Try<T>(out T value, Func<T> action, Action<Exception> handler)
        {
            Contract.Requires(action != null);
            Contract.Requires(handler != null);
            Contract.ValueAtReturn<T>(out value);

            var ret = true;

            try
            {
                value = action();
            }
            catch (Exception ex)
            {
                ret = false;
                handler(ex);
            }

            return ret;
        }
        [DebuggerStepThrough()]
        public static bool Try(Action action)
        {
            Contract.Requires(action != null);

            var ret = true;

            try
            {
                action();
            }
            catch
            {
                ret = false;
            }

            return ret;
        }
        /// <summary>
        /// Executes an action based on the condition specified
        /// </summary>
        /// <param name="condition">condition to evaluate</param>
        /// <param name="actionIfTrue">action to be executed if the condition is true</param>
        /// <param name="actionifFalse">action to be executed if the condition is false</param>
        [System.Diagnostics.DebuggerStepThrough()]
        public static void ExecuteIf(Func<bool> condition, Action actionIfTrue, Action actionifFalse)
        {
            Contract.Requires(condition != null);
            Contract.Requires(actionifFalse != null);
            Contract.Requires(actionIfTrue != null);

            if (condition())
                actionIfTrue();
            else
                actionifFalse();
        }
    }
}
