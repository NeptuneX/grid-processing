﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleInjector;
using SimpleInjector.Diagnostics;

namespace Grid.Common
{
    public static class SimpleInjectorExtension
    {
        public static void RegisterDisposableTransient<TService, TImplementation>(this Container container)
                        where TImplementation : class, IDisposable, TService
                        where TService : class
        {
            var scoped = Lifestyle.Scoped;
            var reg = Lifestyle.Transient.CreateRegistration<TService, TImplementation>(container);

            reg.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent, "suppressed.");

            container.AddRegistration(typeof(TService), reg);
            container.RegisterInitializer<TImplementation>(o => scoped.RegisterForDisposal(container, o));
        }
        public static void RegisterDisposableTransient<TConcrete>(this Container container)
            where TConcrete : class, IDisposable
        {
            var scoped = Lifestyle.Scoped;
            var reg = Lifestyle.Transient.CreateRegistration<TConcrete>(container);

            reg.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent, "suppres");

            container.AddRegistration(typeof(TConcrete), reg);
            container.RegisterInitializer<TConcrete>(
                o => scoped.RegisterForDisposal(container, o));
        }

    }
}
