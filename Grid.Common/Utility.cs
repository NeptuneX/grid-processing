﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace Grid.Common
{
    public class Utility
    {
        public static IPAddress LocalHost
        {
            get
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                var ip = host.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
                return ip;
            }
        }

        public static IPEndPoint ToIPEndPoint(string ip, int port)
        {
            Contract.Requires(port >0 && port <32000);
            Contract.Ensures(Contract.Result<IPEndPoint>() != null);

            return new IPEndPoint(IPAddress.Parse(ip), port);
        }
        public static IPEndPoint ToIPEndPoint(string endPoint)
        {
            Contract.Requires(endPoint != null);
            Contract.Ensures(Contract.Result<IPEndPoint>() != null);

            endPoint = endPoint.Replace("*", IPAddress.Loopback.ToString());

            string[] ep = endPoint.Split(':');
            if (ep.Length < 2) throw new FormatException("Invalid endpoint format");
            IPAddress ip;
            if (ep.Length > 2)
            {
                if (!IPAddress.TryParse(string.Join(":", ep, 0, ep.Length - 1), out ip))
                {
                    throw new FormatException("Invalid ip-adress");
                }
            }
            else
            {
                if (!IPAddress.TryParse(ep[0], out ip))
                {
                    throw new FormatException("Invalid ip-adress");
                }
            }
            int port;
            if (!int.TryParse(ep[ep.Length - 1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out port))
            {
                throw new FormatException("Invalid port");
            }
            return new IPEndPoint(ip, port);

        }
        public static bool IsPortOpen(int port)
        {
            var listeners = IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners();
            return listeners.Any(x => x.Port == port);

            // This piece of shit Always fails
            // var ret = false;
            //using (TcpClient tcpClient = new TcpClient())
            //{
            //    try
            //    {
            //        tcpClient.Connect(IPAddress.Loopback, port);
            //        ret = true;
            //    }
            //    catch
            //    {

            //    }
            // return ret;
        }

        public static int GetRandomOpenPort()
        {
            var startPort=1000;
            var tcpEndPoints = IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners();
            var usedPorts = tcpEndPoints.Select(p => p.Port).OrderBy(x=>x).ToList<int>();
            return Enumerable.Range(startPort, usedPorts.Count *2).Where(port => !usedPorts.Contains(port)).FirstOrDefault();
        }
    



        public static byte[] Serialize(object arg)
        {
            Contract.Requires(arg != null);
            Contract.Ensures(Contract.Result<byte[]>() != null && Contract.Result<byte[]>().Length > 0);


            byte[] ret = null;
            using (var ms = new MemoryStream())
            {
                var bs = new BinaryFormatter();
                bs.Serialize(ms, arg);
                ret = ms.ToArray();
            }

            return ret;
        }
        public static T Deserialize<T>(byte[] arg)
        {
            Contract.Requires(arg != null && arg.Length > 0);
            Contract.Ensures(Contract.Result<T>() != null);

            var ret = default(T);
            using (var ms = new MemoryStream(arg))
            {
                   var serializer = new BinaryFormatter();
                   ret = (T)serializer.Deserialize(ms);
            }
            return ret;
        }

        public static string GetTemporaryPath()
        {
            var tempPath = Path.Combine(Path.GetTempPath(), "Grid Storage");

            var directory = new DirectoryInfo(tempPath);
            if (!directory.Exists)
                directory.Create();

            return directory.FullName;
        }

        public static bool IsDebug
        {
            get
            {
                var ret = false;

                var assembly = Assembly.GetExecutingAssembly();
                object[] attributes = assembly.GetCustomAttributes(typeof(DebuggableAttribute), true);
                if(attributes.Length != 0)
                {
                    var d = (DebuggableAttribute) attributes[0];
                    if (d.IsJITTrackingEnabled) ret = true;
                }
                return ret;
            }
        }
       
    }
}
