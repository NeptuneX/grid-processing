﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading;


namespace Grid.Communicator.Common
{
    public class DataBuffer<T> : IObjectBuffer 
        where T:class
    {
        readonly ConcurrentQueue<string> _failed;
        
        readonly ConcurrentDictionary<string, T[]> _transaction;

        readonly Cyotek.Collections.Generic.CircularBuffer<T> _buffer;
        readonly ReaderWriterLockSlim _lock;
        readonly static object mylock = new object();

        readonly bool _writeAllOrNone;

        public DataBuffer(int capacity, bool overwriteable, bool writeAllorNone)
        {
            _writeAllOrNone = writeAllorNone;
            _buffer = new Cyotek.Collections.Generic.CircularBuffer<T>(capacity);
            _buffer.AllowOverwrite = overwriteable;
            _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

            _transaction = new ConcurrentDictionary<string, T[]>(4, 256);
            _failed = new ConcurrentQueue<string>();
        }

        public bool IsEmpty 
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return  _buffer.IsEmpty && _failed.IsEmpty;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }
        public bool IsFull
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return _buffer.IsFull || ( _transaction.Where(x=>_failed.Contains(x.Key)).Sum(x=>x.Value.Count()) >= _buffer.Capacity);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public int Vacancy
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return _buffer.Capacity - _buffer.Size;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public int Count
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return _buffer.Size;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public int Capacity
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return _buffer.Capacity;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public void Commit(string id)
        {
            if (_transaction.ContainsKey(id))
            {
                var objects = (T[])null;
                _transaction.TryRemove(id, out objects);
                GC.SuppressFinalize(objects);
            }
        }
        public void Rollback(string id)
        {
            if (_transaction.ContainsKey(id))
                _failed.Enqueue(id);
        }


        public T[] Read(string id, int maxObjects)
        {
            Contract.Requires(id != null);
            Contract.Requires(maxObjects >0);

            var objs = Read(maxObjects);
            _transaction.TryAdd(id, objs);
            return objs;
        }
        public T[] Read(int maxObjects)
        {
            try
            {
                _lock.EnterWriteLock();
                HandleFailedTransactions();
                var count = Math.Min(_buffer.Count(), maxObjects);
                return _buffer.Get(count);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        private void HandleFailedTransactions()
        {
            T[] objects = null;

            lock (this)
            {
                var id = string.Empty;
                if (_failed.TryDequeue(out id))
                {
                    if (_transaction.TryRemove(id, out objects))
                    {
                        var written = 0;
                        if (SecureWrite(objects, out written))
                        {
                            //rolled back successfully
                        }
                        else
                        {
                            objects = objects.Skip(written).ToArray<T>();
                            _transaction.TryAdd(id, objects);
                            _failed.Enqueue(id);
                        }
                    }
                    else
                        _failed.Enqueue(id);
                } 
            }
        }
        private bool SecureWrite(T[] objects, out int written)
        {
            lock (this)
            {

                var count = _buffer.Capacity - _buffer.Count();
                written = Math.Min(count, objects.Length);

                if (count >= objects.Length)
                    _buffer.Put(objects);
                else
                {
                    if (!_writeAllOrNone)
                        _buffer.Put(objects.Take(written).ToArray<T>());
                    else
                    {
                        written = objects.Length; //None written due to flag;
                    }
                }
                return written == objects.Length;
            }
        }


        public bool Write(T[] objects, out int written)
        {
            if (!_failed.IsEmpty)
                HandleFailedTransactions();

            return SecureWrite(objects, out written);
        }

       
        public int Write(T[] objects)
        {
            int written = 0;
            Write(objects, out written);
            return objects.Length-written;
        }


        object[] IObjectBuffer.Read(string id, int maxObjects)
        {
            return Read(id, maxObjects);
        }
        bool IObjectBuffer.Write(object[] objs, out int written)
        {
            var arr = Array.CreateInstance(typeof(T), objs.Length) as T[];
            Array.Copy(objs, arr, objs.Length);

            return Write(arr, out written);
        }
        int IObjectBuffer.Write(object[] objs)
        {
            var arr = Array.CreateInstance(typeof(T), objs.Length) as T[];
            Array.Copy(objs, arr, objs.Length);

            return Write(arr);
        }
        object[] IObjectBuffer.Read(int maxObjects)
        {
            return Read(maxObjects);
        }
    }
}
