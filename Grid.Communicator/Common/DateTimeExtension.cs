﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Communicator.Common
{
    public static class DateTimeExtension
    {
        public static bool IsEmpty(this DateTime self) => DateTime.Compare(self, DateTime.MinValue)==0;
    }
}
