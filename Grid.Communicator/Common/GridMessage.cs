﻿using System;
using System.Diagnostics.Contracts;
using System.Net;
using NetworkChannel;

namespace Grid.Communicator.Common
{
    class GridMessage
    {
        public readonly GridSignal Command;
        public readonly IPEndPoint Sender;
        public readonly DateTime SentAt;
        public readonly object[] Arguments;

        public GridMessage(CommandMessage msg)
        {
            Sender = msg.Sender;
            Command = (GridSignal) msg.Command;
            SentAt = (DateTime)msg.Arguments[0];

            if (msg.Arguments.Length>1)
            {
                Arguments = new object[msg.Length-1];
                Array.Copy(msg.Arguments, 1, Arguments, 0, Arguments.Length);
            }
            else
            {
                Arguments = new object[0];
            }
        }
        public T Get<T>(int idx)
        {
            Contract.Requires(idx<Arguments.Length);

            var ret = default(T);
            ret = (T) Arguments[idx];
            return ret;
        }
    }
}
