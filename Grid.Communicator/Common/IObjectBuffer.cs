﻿using System.Collections.Generic;

namespace Grid.Communicator.Common
{
    public interface IObjectBuffer
    {
        int Vacancy { get; }
        int Count { get; }
        int Capacity { get; }

        bool IsEmpty { get; }
        bool IsFull { get; }

        void Commit(string id);
        object[] Read(string id, int maxObjects);
        object[] Read(int maxObjects);
        void Rollback(string id);
        bool Write(object[] objs, out int written);
        int Write(object[] objs);
    }
}