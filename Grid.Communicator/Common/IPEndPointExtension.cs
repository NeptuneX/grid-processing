﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Text;

namespace Grid.Communicator.Common
{
    public static class IPEndPointExtension
    {
        public static IPEndPoint FromString(this IPEndPoint self, string text)
        {
            IPEndPoint ret = null;

            try
            {
                var parts = text.Split(new char[] { '/', '\\', '*', ':' }, StringSplitOptions.RemoveEmptyEntries);

                if (parts[0] != "tcp")
                {
                    var port = int.Parse(parts.Last());
                    var ip = parts[parts.Length - 2];

                    if (ip == "*")
                        ip = "127.0.0.1";

                    ret = new IPEndPoint(IPAddress.Parse(ip), port);
                }
            }
            finally
            {

            }

            return ret;
        }
    }
}
