﻿using System;
using Grid.Communicator.Common;

namespace Grid.Communicator
{
    public sealed class CommunicationPolicy
    {
        public TimeSpan MessageTimeout { get; }
        public TimeSpan MaxIdleTime { get; }
        public int MaxRetries { get; }
        public int MaxObjectsToSend { get; }

        public CommunicationPolicy(TimeSpan messageTimeout, TimeSpan idletime, int maxretries, int maxobjects)
        {
            MessageTimeout = messageTimeout;
            MaxIdleTime = idletime;
            MaxRetries = maxretries;
            MaxObjectsToSend = maxobjects;
        }

        //TimeSpan ElapsedSpan(DateTime dtPast)
        //{
        //    DateTime dtNow = DateTime.UtcNow;
        //    return dtNow.ToUniversalTime().Subtract(dtPast.ToUniversalTime());
        //}

        public bool IsTimedOut(DateTime last)
        {
            var ret = false;

            if(last != DateTime.MinValue)
            {
                ret = DateTime.UtcNow.Subtract(last).TotalMilliseconds >= this.MessageTimeout.TotalMilliseconds;
            }
            return ret;
        }
        public bool IsRetryAllowed(int attempts)
        {
            return attempts < MaxRetries;
        }
        public bool IsIdle(DateTime lastTime)
        {
            return DateTime.UtcNow.Subtract(lastTime).TotalMilliseconds >= MaxIdleTime.TotalMilliseconds;
        }

        public static CommunicationPolicy Default => new CommunicationPolicy(TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(60), 3, 4000);
        public override string ToString()
        {
            return $"Message duration: {MessageTimeout.ToReadableString()}\n Idle duration: {MaxIdleTime.ToReadableString()}\n Maximum Retries: {MaxRetries}\n Object threshold: {MaxObjectsToSend}";
        }
    }
}
