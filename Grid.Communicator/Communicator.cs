﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Threading;
using Grid.Common;
using Grid.Common.Process;
using Grid.Communicator.Common;
using NetworkChannel;

namespace Grid.Communicator
{
    public class Communicator: IDisposable
    {
        readonly CommunicationPolicy KPolicy;
        //readonly IPEndPoint[] _host;
        readonly IPEndPoint _host;

        readonly List<Tuple<SinkControlBlock, NetworkSignalChannel>> _sinks;
        readonly SourceManager _machine;
        

        readonly DataSink _receiver;
        readonly IJournal _journal;
        readonly ManualResetEventSlim _signal;

        readonly IObjectBuffer _outBuffer, _inBuffer;

        public Communicator(IJournal journal, ManualResetEventSlim signal, CommunicationPolicy policy, IPEndPoint host, IObjectBuffer receivingBuffer, IObjectBuffer sendingBuffer)
        {
            Contract.Requires(host != null);
            Contract.Requires(journal != null);

            _journal = journal;

            _host = host;
            //if (IPAddress.IsLoopback(host.Address))
            //{
            //    var list = new List<IPEndPoint>();
            //    foreach (var tmp in Dns.GetHostByName(Dns.GetHostName()).AddressList)
            //    {
            //        list.Add(new IPEndPoint(tmp, host.Port));
            //    }
            //}
            //else
            //    _host = new IPEndPoint[] { host };

            _signal = signal;
            _journal.Debug($"Applying communication policy...");
            KPolicy = policy;
            _journal.Debug(KPolicy.ToString());

            //_journal.Debug("Opening network buffers ....");
            _inBuffer = receivingBuffer;
            _outBuffer = sendingBuffer;

            _journal.Debug($"Initializing receiving socket at {host.ToString()}");
            _receiver = new DataSink(journal, KPolicy, _inBuffer, _host);

            _sinks = new List<Tuple<SinkControlBlock, NetworkSignalChannel>>();
            _machine = new SourceManager(_journal, _host,  KPolicy, _outBuffer);

            _journal.Debug($"Communicator initialization done.");
        }

        //public IObjectBuffer Received => _inBuffer;
        //public IObjectBuffer Sent => _outBuffer;

        public void Run()
        {
            try
            {
                while (!_signal.IsSet)
                {
                    _receiver.Run();
                    foreach (var it in _sinks)
                    {
                        if (it.Item1.State != SinkState.Stop)
                            _machine.Run(it.Item1, it.Item2);
                    }
                    if (_sinks.Any(x => x.Item1.State == SinkState.Stop))
                        _sinks.RemoveAll(x => x.Item1.State == SinkState.Stop);
                }
            }
            catch (Exception ex)
            {
                if (!(ex is ThreadInterruptedException))
                    _journal.Error(ex);
            }
        }
        public void AddSink(IPEndPoint point)
        {
            Contract.Requires(point != null);

            var block = new SinkControlBlock(point);
            var channel = new NetworkSignalChannel(_host, point);

            block.State = SinkState.Start;

            _sinks.Add(new Tuple<SinkControlBlock, NetworkSignalChannel>(block, channel));
        }


        //IEnumerable<IPEndPoint> Sinks => _sinks.Select(x => x.Item1.Address);

        public void Dispose() {
            _journal.Info($"Shutting down communicator...");

            _journal.Debug($"Closing receiving channel...");
            _receiver.Dispose();

            _journal.Debug($"Closing sending channels ...");
            foreach (var it in _sinks)
                _machine.Stop(it.Item1, it.Item2);
            _sinks.Clear();

            _journal.Debug($"Checking buffers for leftovers...");
            if (!_inBuffer.IsEmpty)
            {
                _journal.Debug($"Flusing {_inBuffer.Count} unprocessed item(s) to disk...");
                //Persist to Holder.InBox
            }

            if (!_outBuffer.IsEmpty)
            {
                _journal.Debug($"Flusing {_outBuffer.Count} unsent item(s) to disk...");
                //Persist to Holder.Outbox

            }

            _journal.Debug($"Communicator shutdown complete.");
        }
    }
}
