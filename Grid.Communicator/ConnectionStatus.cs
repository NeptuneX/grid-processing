﻿using System;
using System.Net;

namespace Grid.Communicator
{
    public class ConnectionStatus
    {
        public DateTime CreatedAt { get; }
        public IPEndPoint Address { get; }

        public DateTime LastSentAt { get; private set; }
        public DateTime LastReceivedAt { get; private set; }
        public DateTime LastActivityAt { get; private set;  }
        public int State { get; private set; }
        public int RetryAttempts { get; private set; }
        public GridSignal LastMessage { get; private set; }
        public int Capacity { get; set; }

        
        public ConnectionStatus(IPEndPoint endPoint, int startState)
        {
            CreatedAt = DateTime.UtcNow;
            LastActivityAt = CreatedAt;
            Address = endPoint;

            State = startState;
            LastMessage = 0;
            LastSentAt = DateTime.MinValue;
            LastReceivedAt = DateTime.MinValue;
            RetryAttempts = 0;
        }

        public void Sent(GridSignal msg)
        {
            LastMessage = msg;
            LastSentAt = DateTime.UtcNow;
            LastActivityAt = LastSentAt;
            RetryAttempts++;
        }
        public void Received()
        {
            LastReceivedAt = DateTime.UtcNow;
            LastActivityAt = LastReceivedAt;
            RetryAttempts = 0;
        }
        public void Change(int newState)
        {
            State = newState;
            
            //LastReceivedAt = DateTime.MinValue;
            //LastSentAt = DateTime.MinValue;

            LastMessage = 0;

            RetryAttempts = 0;

            LastActivityAt = DateTime.UtcNow;
        }

        public bool AnyMessageSent => LastSentAt!= DateTime.MinValue;
        public bool AnyMessageReceived => LastReceivedAt != DateTime.MinValue;

        internal void RegisterDataActivity()
        {
            LastActivityAt = DateTime.UtcNow;
        }
    }
}