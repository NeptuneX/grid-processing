﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;

using Grid.Common;
using Grid.Communicator.Common;
using NetworkChannel;
using NetworkChannel.Common;

namespace Grid.Communicator
{
    public class DataSink: IDisposable
    {
       readonly INetworkChannel _channel;
       readonly IDictionary<string, ConnectionStatus> _sources;

       readonly IObjectBuffer _buffer;
       readonly CommunicationPolicy _policy;

        readonly IJournal _log;

      
        public DataSink(IJournal journal, CommunicationPolicy policy, IObjectBuffer buffer, IPEndPoint host)
        {
            _log = journal;
            _channel = new ChannelReader<DefaultObjectSerializer>(host);
            _sources = new Dictionary<string, ConnectionStatus>();
            _buffer = buffer;
            _policy = policy;
        }


           public void Run() {

            var signals = _channel.ReceiveSignal(_policy.MessageTimeout);
            var filteredSignals = signals.Where(x => (x.Length > 0) && !_policy.IsTimedOut((DateTime) x.Arguments[0]));
            if (filteredSignals.Any())
                ProcessSignals(signals);

            if (_sources.Any(x => x.Value.State == (int) SourceState.Hot))
            {
                var hotSources = _sources.Where(x => x.Value.State == (int) SourceState.Hot);
                Contract.Assert(hotSources.Count() == 1, "more than 1 hot sources");

                var hot = hotSources.FirstOrDefault().Value;
                var hotSender = hotSources.FirstOrDefault().Key;

                var shouldReset = false;
                var data = _channel.ReceiveData(_policy.MessageTimeout);


                if (data.Any())  // received the data so enable other sources 
                {
                    shouldReset = true;

                    foreach (var d in data)
                    {
                        if (d.Sender.Equals(hot.Address))
                        {
                            var written = 0;
                            if (_buffer.Write(d.Data, out written))
                                _channel.SendSignal(d.Sender, (int) GridSignal.RECEIVED, new object[] { DateTime.UtcNow,  d.Key} );
                            else
                                _channel.SendSignal(d.Sender, (int) GridSignal.RETRANSMIT, new object[] { DateTime.UtcNow, d.Key });

                            _log.Debug($"Received data from {d.Sender}");
                        }
                        else
                        {
                            _log.Debug($"data received from unexpected sender: {d.Sender}, discarding!");
                        }
                    }

                    if (_sources.ContainsKey(hotSender))
                    {
                        var source = _sources[hotSender];
                        source.RegisterDataActivity();
                        source.Change((int) SourceState.Connected);
                    }
                }
                else
                {
                    //Determine if timed out?
                    if (_policy.IsTimedOut(hot.LastReceivedAt))
                    {
                        shouldReset = true;   //tell the source to retry if it has anything to send, enable other sources.
                        hot.Change((int) SourceState.Connected);
                        _channel.SendSignal(hot.Address, (int) GridSignal.CONTINUE, new object[] { DateTime.UtcNow });

                        _log.Debug($"Hot source timed out {hotSender}");
                    }
                }
                
                if (shouldReset)  //should enable others?
                {
                    foreach (var it in _sources.Where(x => x.Key != hotSender))
                    {
                        it.Value.Change((int) SourceState.Connected);
                        _channel.SendSignal(Utility.ToIPEndPoint(it.Key), (int) GridSignal.CONTINUE, new object[] { DateTime.UtcNow });
                    }
                }
            }
        }


        private void ProcessSignals(IEnumerable<CommandMessage> signals)
        {
            foreach (var signal in signals)
                switch ((GridSignal) signal.Command)
                {
                    case GridSignal.HELLO:  // a new source is trying to connect.
                        if (!_sources.ContainsKey(signal.Sender.ToString()))
                        {
                            var status = new ConnectionStatus(signal.Sender, (int) SourceState.Connected);
                            _sources.Add(signal.Sender.ToString(), status);
                        }
                        _channel.SendSignal(signal.Sender, (int) GridSignal.HELLO, new object[] { DateTime.UtcNow, $"{System.Diagnostics.Process.GetCurrentProcess().Id}", DateTime.UtcNow, _buffer.Capacity });
                        _log.Debug($"Connected to {signal.Sender}");
                        break;

                    case GridSignal.READY2RECEIVE:   // Send this a greeen signal & tell others to wait, otherwise decline.
                                                     // is there anyone already sending anything and our buffer has space?
                        var active = _sources.Where(x => x.Value.State == (int) SourceState.Hot).Select(x => x.Value).FirstOrDefault();
                        if (active == null)
                        {
                            // No one sending data
                            var hot = signal.Sender;
                            _sources[hot.ToString()].Change((int) SourceState.Hot);

                            var dataPoint = (_channel as ChannelReader<DefaultObjectSerializer>)?.Host.Data;
                            if (dataPoint != null)
                                _channel.SendSignal(hot, (int) GridSignal.READY, new object[] { DateTime.UtcNow, dataPoint.ToString(), Math.Min(_policy.MaxObjectsToSend, _buffer.Vacancy) });
                            else
                                _log.Error($"could not retrieve data end point for source {signal.Sender}");

                            //set all others to wait
                            var list = _sources.Where(x => x.Key != hot.ToString()).Select(x => x.Value);
                            foreach (var it in list)
                            {
                                _channel.SendSignal(it.Address, (int) GridSignal.WAIT, new object[] { DateTime.UtcNow });
                                it.Change((int) SourceState.Wait);
                            }

                            _log.Debug($"Data channel started by {signal.Sender}");
                        }
                        else if (active.Address.Equals(signal.Sender.Address))
                        {
                            _log.Warn($"{signal.Sender.ToString()} requested data channel that is already active. request ignored!");
                        }
                        else
                        {
                            //some source is already sending data
                            _channel.SendSignal(signal.Sender, (int) GridSignal.WAIT, new object[] { DateTime.UtcNow });
                            _log.Debug($"Request by {signal.Sender} is put on hold, {active.Address.ToString()} is already active");
                        }
                        break;

                    case GridSignal.NOTHING2SEND:
                        _sources[signal.Sender.ToString()].Change((int) SourceState.Connected);
                        ResetOtherSources(signal.Sender);
                        _log.Debug($"Data channel closed by {signal.Sender}");
                        break;

                    case GridSignal.AREUALIVE:
                        _channel.SendSignal(signal.Sender, (int) GridSignal.IMALIVE, new object[] { DateTime.UtcNow });

                        _log.Debug($"Pinged by {signal.Sender}");
                        break;

                    case GridSignal.GOODBYE:
                        _sources.Remove(signal.Sender.ToString());

                        _log.Debug($"Stopped {signal.Sender}");
                        break;
                }
        }

        private void ResetOtherSources(IPEndPoint active)
        {
            foreach (var it in _sources.Where(x => x.Key != active.ToString()))
            {
                _channel.SendSignal(Utility.ToIPEndPoint(it.Key), (int) GridSignal.CONTINUE, new object[] { DateTime.UtcNow });
                it.Value.Change((int) SourceState.Connected);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach(var source in _sources)
                    {
                        
                    }
                    _channel.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }



        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
