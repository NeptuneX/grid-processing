﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Grid.Common;
using Grid.Communicator.Common;
using NetworkChannel;
using NetworkChannel.Common;

namespace Grid.Communicator
{
    public class DataSource: IDisposable
    {
        readonly CommunicationPolicy _policy;
        readonly IObjectBuffer _buffer;

        protected readonly INetworkChannel _channel;
        readonly ConnectionStatus _sink;

        Stack<string> _stack;
        readonly IJournal _log;


        public DataSource(IJournal journal, CommunicationPolicy policy, IObjectBuffer source, IPEndPoint host, IPEndPoint target)
        {
            _log = journal;
            _buffer = source;
            _policy = policy;

            _channel = new ChannelWriter<DefaultObjectSerializer>(host, target);
            _sink = new ConnectionStatus(target, (int)SinkState.Start);
            _sink.Capacity = _policy.MaxObjectsToSend;

            _stack = new Stack<string>();
        }

        public void Run()
        {
            //try receiving signals but discard the timedout signals, if any
            var signals = _channel.ReceiveSignal(_policy.MessageTimeout);
                    //.Where(x => !_policy.IsTimedOut((DateTime) x.Arguments[0]));
            if(signals.Any())
                ProcessSignals(signals);

            SendingCycle();
        }
        private void SendingCycle()
        {
            var state = (SinkState)_sink.State;
            var signalToSend = GridSignal.Default;
            var sentMessage = (GridSignal)_sink.LastMessage;

            switch (state)
            {
                case SinkState.Start:
                    if (sentMessage != GridSignal.HELLO) //First Time.
                        signalToSend = GridSignal.HELLO;
                    else if(_policy.IsTimedOut(_sink.LastSentAt))
                    {
                        if (_policy.IsRetryAllowed(_sink.RetryAttempts)) // Retries
                                signalToSend = GridSignal.HELLO;
                         else
                              ChangeState(SinkState.Stop);
                    }
                    break;

                case SinkState.Connected:
                    if (!_buffer.IsEmpty)
                    {
                        if (sentMessage != GridSignal.READY2RECEIVE) //First Time.
                            signalToSend = GridSignal.READY2RECEIVE;
                        else if(_sink.LastMessage == GridSignal.READY2RECEIVE && _policy.IsTimedOut(_sink.LastSentAt)) 
                        {
                                if (_policy.IsRetryAllowed(_sink.RetryAttempts))
                                    signalToSend = GridSignal.READY2RECEIVE;
                                else  // check if the sink is still there
                                    ChangeState(SinkState.Start);
                           
                        }
                    } 
                    else if(_policy.IsIdle(_sink.LastReceivedAt)) // has it been in connected state for a long time?
                         ChangeState(SinkState.Idle);
                    break;

                case SinkState.Ready:
                    if (!_buffer.IsEmpty)  // is the data still there?
                    {
                        var size = Math.Min(_policy.MaxObjectsToSend, _sink.Capacity);
                        var key = Guid.NewGuid().ToString();
                        var objs = _buffer.Read(key, size);
                        _stack.Push(key);

                        _channel.SendData(key, objs);
                        ChangeState(SinkState.AwaitingConfirmation);

                        _log.Debug($"sent {objs.Count()}");
                    }
                    else
                    {
                        // data is gone
                        ChangeState(SinkState.Connected);
                        signalToSend = GridSignal.NOTHING2SEND;
                    }
                    break;

                case SinkState.AwaitingConfirmation:
                    if (_policy.IsTimedOut(_sink.LastReceivedAt))
                    {
                        var tmp = _stack.Count > 0 ? _stack.Pop(): string.Empty;
                        if (!string.IsNullOrEmpty(tmp))
                            _buffer.Rollback(tmp);
                        ChangeState(SinkState.Connected);
                    }
                    break;

                case SinkState.Stop:
                    break;

                case SinkState.Idle:
                    if (_policy.IsIdle(new DateTime(Math.Min(_sink.LastSentAt.Ticks, _sink.LastReceivedAt.Ticks))) )
                    {
                        if (sentMessage != GridSignal.AREUALIVE) //First Time.
                        {
                            signalToSend = GridSignal.AREUALIVE;
                        }
                        else if (_sink.LastMessage == GridSignal.AREUALIVE && _policy.IsTimedOut(_sink.LastSentAt))
                        {
                            if (_policy.IsRetryAllowed(_sink.RetryAttempts))
                            {
                                signalToSend = GridSignal.AREUALIVE;
                            }
                            else
                                ChangeState(SinkState.Stop);
                        } 
                    }
                    break;

                case SinkState.Wait:
                    if (_policy.IsIdle(new DateTime(Math.Min(_sink.LastSentAt.Ticks, _sink.LastReceivedAt.Ticks))))
                    {
                        ChangeState(SinkState.Start);
                    }
                    break;
            }

            if (signalToSend != GridSignal.Default)
            {
                _channel.SendSignal((int)signalToSend, new object[] { _sink.LastSentAt});
                _sink.Sent(signalToSend);

                _log.Debug($"Sending {Enum.GetName(typeof(GridSignal), signalToSend)}");
            }

        }
        private void ProcessSignals(IEnumerable<CommandMessage> signals)
        {
                foreach (var signal in signals)
                {
                    switch ((GridSignal)signal.Command)
                    {
                        case GridSignal.HELLO:
                            switch ((SinkState) _sink.State)
                            {
                                case SinkState.Start:
                                    ChangeState(SinkState.Connected);
                                _log.Debug($"sink replied. Connected");
                                break;

                                case SinkState.AwaitingConfirmation:
                                    if (_stack.Count > 0)
                                    {
                                        var key = _stack.Pop();
                                        if(!string.IsNullOrEmpty(key))
                                            _buffer.Rollback(string.Empty);
                                    }
                                    ChangeState(SinkState.Connected);
                                _log.Debug($"rolling back the packets sent");
                                break;
                                default:
                                    ChangeState(SinkState.Connected);
                                _log.Debug($"sink replied. Connected");
                                break;
                            }
                            break;

                        case GridSignal.READY:
                        _log.Debug($"sink ready to receive with capacity:{signal.Arguments[1].ToString()}");
                        if (_sink.State == (int) SinkState.Connected || _sink.State == (int) SinkState.Ready)
                            {
                                var tmp = 0;
                                if(signal.Length > 1 && int.TryParse(signal.Arguments[1].ToString(), out tmp))
                                    _sink.Capacity = tmp;
                                ChangeState(SinkState.Ready);
                            }
                            else
                                ChangeState(SinkState.Connected);
                            break;

                        case GridSignal.CONTINUE:
                        //if the sink said wait earlier, it will be connected again for operations, if it was in hot state then the state is reset to connected
                        _log.Debug($"sink replied with continue");
                        ChangeState(SinkState.Connected);
                            break;

                        case GridSignal.IMALIVE:
                        _log.Debug($"sink pings");
                        ChangeState(SinkState.Connected);
                            break;

                        case GridSignal.WAIT:
                        _log.Debug($"sink asks to wait");
                        if (IsAnyState(SinkState.Connected, SinkState.Ready))
                                ChangeState(SinkState.Wait);
                            break;

                        case GridSignal.STOP:
                        _log.Debug($"sink drops the connection, stopping");
                        if (_sink.State != (int) SinkState.Stop)
                                ChangeState(SinkState.Stop);
                            break;


                        case GridSignal.RECEIVED:
                        _log.Debug($"sink confirms the receiving of data");
                        if (_sink.State == (int)SinkState.AwaitingConfirmation)
                            {
                                var clientKey = signal.Length >=2? signal.Arguments[1].ToString(): "";
                                var serverKey = _stack.Count > 0 ? _stack.Pop() : "";

                                if (clientKey == serverKey)
                                    _buffer.Commit(serverKey);
                                else if (!string.IsNullOrEmpty(serverKey))
                                    _buffer.Rollback(serverKey);

                                ChangeState(SinkState.Connected);
                            }
                            else // Ignore this message at other states   ---  if(IsAnyState(SinkState.Connected, SinkState.Ready))
                            {
                                //Error
                                ChangeState(SinkState.Stop);
                            }
                            break;
                        case GridSignal.RETRANSMIT:
                        _log.Debug($"sink lost the data");
                        if (_sink.State == (int)SinkState.AwaitingConfirmation)
                            {
                                var clientKey = signal.Length >= 2 ? signal.Arguments[1].ToString() : "";
                                var serverKey = _stack.Pop();
                                _buffer.Rollback(serverKey);
                                ChangeState(SinkState.Ready);
                            }
                            else if (IsAnyState(SinkState.Ready, SinkState.Idle))
                                ChangeState(SinkState.Ready);
                            break;
                        case GridSignal.GOODBYE:
                        _log.Debug($"sink stops");
                        ChangeState(SinkState.Stop);
                            break;
                    }
                _sink.Received();
            }
        }

        void ChangeState(SinkState state)
        {
            _sink.Change((int) state);
            _log.Debug($"Changed to {Enum.GetName(typeof(SinkState), state)}");
        }

        private bool IsAnyState(params SinkState[] others) => others.Contains((SinkState)_sink.State);

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _channel.SendSignal((int) GridSignal.GOODBYE, new object[] { DateTime.UtcNow });
                    _channel.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
