﻿namespace Grid.Communicator
{
    public enum GridSignal
    {
        Default = -1,

        NO = 0,
        YES = 1,
        ACK = 2,

        HELLO = 10,
        GOODBYE = 11,
        AREUALIVE = 12,
        IMALIVE = 13,

        READY2RECEIVE = 21,
        READY = 22,
        RECEIVED = 23,
        RETRANSMIT = 24,
        NOTHING2SEND = 25,

        WAIT = 41,
        STOP = 42,
        CONTINUE = 43
    }

}