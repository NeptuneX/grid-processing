﻿

namespace Grid.Communicator
{
    public enum SinkState : byte
    {
        None = 0,

        Start = 1,

        Connected = 11,

        Ready = 20,
        AwaitingConfirmation = 21,

        Wait = 30,
        Idle = 31,

        Stop = 10

    }

    public enum SourceState : byte
    {
        None =0,

        Connected = 11,

        Hot = 20,
        Wait = 30,

        Stop = 10
    }
}