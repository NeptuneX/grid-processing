﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Text;
using NetworkChannel;

namespace Grid.Communicator
{

    internal struct SinkIdentity
    {
        internal readonly IPEndPoint address;
        internal readonly string name;
        internal readonly DateTime createdAt;
        internal readonly int capacity;

        public SinkIdentity(IPEndPoint ip, string txt, DateTime date, int space)
        {
            address = ip;
            name = txt;
            createdAt = date;
            capacity = space;
        }

        public SinkIdentity(IPEndPoint ip)
        {
            address = ip;
            name = typeof(SinkIdentity).Name;
            createdAt = DateTime.UtcNow;
            capacity = -1;
        }
    }

    internal class SinkControlBlock
    {
        SinkState _state;

        internal IPEndPoint Address { get; }
        internal DateTime LastSentAt {get; set;}
        internal DateTime LastReceivedAt {get; set;}
        //internal DateTime LastDataAt {get; set;}
        internal SinkState State
        {
            get { return _state; }
            set
            {
                Retries = 0;
                _state = value;
            }
        }
        internal bool IsListening {get; set;}
        internal bool IsActive { get; set; }
        internal short  Retries {get; set;}
        internal int Capacity { get; set; }
        internal string Name { get;  set; }
        internal DateTime CreatedAt { get; set; }

        internal INetworkDataChannel DataChannel { get; set; } = null;

        internal SinkControlBlock(IPEndPoint point)
        {
            Contract.Requires(point != null);

            Address = point;

            LastSentAt = DateTime.MinValue;
            LastReceivedAt = DateTime.MinValue;
            //LastDataAt = DateTime.MinValue;


            State = SinkState.None;
            IsListening = false;
            IsActive = false;
            Retries = 0;
        }

    }
}
