﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Threading;
using Grid.Common;
using Grid.Communicator.Common;
using NetworkChannel;

namespace Grid.Communicator
{
    internal struct DataTransaction
    {
        internal readonly IPEndPoint address;
        internal readonly string key;
        internal readonly DateTime sentAt;

        internal DataTransaction(IPEndPoint pt, string id, DateTime time)
        {
            address = pt;
            key = id;
            sentAt = time;
        }
        public override bool Equals(object obj)
        {
            var data = (DataTransaction)obj;
            return address.Equals(data.address) && key ==data.key;
        }
    }
    internal class SourceManager
    {
        readonly IJournal _journal;
        readonly IObjectBuffer _buffer;
        readonly CommunicationPolicy _policy;
        readonly List<DataTransaction> _transactions;

        //INetworkDataChannel _dataChannel;
        readonly IPEndPoint _host;

        public SourceManager(IJournal journal, IPEndPoint host, CommunicationPolicy policy, IObjectBuffer buffer)
        {
            Contract.Requires(journal != null);
            Contract.Requires(buffer != null);
            Contract.Requires(policy != null);
            Contract.Requires(host != null);

            _journal = journal;

            _host = host;
            _buffer = buffer;
            _policy = policy;
            _transactions = new List<DataTransaction>(32);
        }


        internal void Run(SinkControlBlock sink, NetworkSignalChannel channel)
        {
            Contract.Requires(sink != null);
            Contract.Requires(channel != null);

            if (sink.IsListening )
            {
                var signals = channel.ReceiveSignal(_policy.MessageTimeout);

                if (signals.Any())
                {
                    var filtered = signals.Select(x => new GridMessage(x));
                    filtered = filtered.Where(x => !_policy.IsTimedOut(x.SentAt));
                    sink.LastReceivedAt = filtered.Max(x => x.SentAt);
                    ReceivingCycle(sink, filtered);
                }
                else
                {
                    //Check if listening and last sent was after last received and it has been timed out
                    if (DateTime.Compare(sink.LastReceivedAt, sink.LastSentAt) < 0 && _policy.IsTimedOut(sink.LastReceivedAt))
                    {
                        if (_policy.IsRetryAllowed(sink.Retries))
                        {
                            SendingCycle(sink, channel);
                        }
                        else
                        {
                            if(sink.IsActive)
                                StopDataCommunication(sink);
                            sink.State = sink.State != SinkState.Start? SinkState.Start: SinkState.Stop;
                            if (sink.State == SinkState.Stop)
                            {
                                _journal.Debug($"sink {sink.Address.ToString()} is dead");
                                Rollback(sink.Address);
                                sink.IsListening = true;
                            }
                        }
                    }
                }
            }
            else
            {
                SendingCycle(sink, channel);
            }

            if (sink.IsActive && sink.DataChannel != null)
            {
                if (!_buffer.IsEmpty)
                {
                    SendData(sink);
                    Thread.Sleep(TimeSpan.FromMilliseconds(500));   // otherwise sink does not receive anything
                }
                else
                {
                    // data is gone
                    channel.SendSignal((int) GridSignal.NOTHING2SEND, new object[] { DateTime.UtcNow });
                    sink.IsListening = _transactions.Any(x => x.address.Equals(sink.Address) && !_policy.IsTimedOut(x.sentAt));
                    sink.State = sink.IsListening ? SinkState.AwaitingConfirmation : SinkState.Connected;
                    StopDataCommunication(sink);

                    _journal.Debug($"No more data, data channel closed with {sink.Address.ToString()}");
                }

            }
            //Any data that is sent but no ack received and the wait has timed out should be rolledback
            RollbackTimedOut();
        }
        internal void Stop(SinkControlBlock sink, NetworkSignalChannel channel)
        {
            Contract.Requires(sink != null);
            Contract.Requires(channel != null);

            if (sink.IsActive)
            {
                StopDataCommunication(sink);
            }

            channel.SendSignal((int) GridSignal.GOODBYE, new object[] { DateTime.UtcNow });
            sink.State = SinkState.Stop;

            var list = _transactions.Where(x => x.address == sink.Address);

            foreach (var it in list.Where(x => !_policy.IsTimedOut(x.sentAt)))
                _buffer.Commit(it.key);

            foreach (var it in list.Where(x => _policy.IsTimedOut(x.sentAt)))
                _buffer.Rollback(it.key);
        }

  

        private void SendingCycle(SinkControlBlock sink, NetworkSignalChannel channel)
        {
            var signalToSend = GridSignal.Default;
            var arguments = new List<object>(4);

            try
            {
                switch (sink.State)
                {
                    case SinkState.Start:
                        signalToSend = GridSignal.HELLO;
                        arguments.AddRange(new object[] { System.Diagnostics.Process.GetCurrentProcess().Id.ToString(), DateTime.UtcNow, _buffer.Capacity });
                        sink.IsListening = true;
                        break;

                    case SinkState.Connected:
                        if (!_buffer.IsEmpty)
                        {
                            signalToSend = GridSignal.READY2RECEIVE;
                            arguments.Add(_buffer.Count);
                            sink.IsListening = true;
                        }
                        else if (_policy.IsIdle(sink.LastSentAt)) // has it been in connected state for a long time?
                        {
                            sink.State = SinkState.Idle;
                            sink.IsListening = false;
                        }
                        break;

                    case SinkState.Ready:
                        if (_buffer.IsEmpty)
                        {
                            //handled outside at data sending
                        }
                        break;

                    case SinkState.Idle:
                        signalToSend = GridSignal.AREUALIVE;
                        sink.IsListening = true;
                        break;
                }

                if (signalToSend != GridSignal.Default)
                {
                    sink.LastSentAt = DateTime.UtcNow;
                    arguments.Insert(0, sink.LastSentAt); //first argument must be date
                    channel.SendSignal((int) signalToSend,  arguments.ToArray());
                    sink.Retries += 1;
                    _journal.Debug($"Sending {Enum.GetName(typeof(GridSignal), signalToSend)} to {sink.Address}");
                }
            }
            catch (Exception ex)
            {
                _journal.Error(ex, $"Sending fails for {sink.Address.ToString()}");
            }
        }
        private void ReceivingCycle(SinkControlBlock sink, IEnumerable<GridMessage> signals)
        {
            _journal.Debug($"received {signals.Count()} from {sink.Address.ToString()}");
            try
            {
                foreach (var signal in signals)
                {
                    sink.IsListening = false;
                    switch (signal.Command)
                    {
                        case GridSignal.HELLO:
                            if (sink.State == SinkState.AwaitingConfirmation)
                                Rollback(sink.Address);
                            else
                            {
                                sink.State = SinkState.Connected;
                                sink.Name = signal.Get<string>(0);
                                sink.CreatedAt = signal.Get<DateTime>(1);
                                sink.Capacity = signal.Get<int>(2);
                            }
                            _journal.Debug($"sink {sink.Name} at {sink.Address} is connected");
                            break;

                        case GridSignal.READY:
                            if (!_buffer.IsEmpty)
                            {
                                if (sink.DataChannel == null)
                                {
                                    var ip = Utility.ToIPEndPoint(signal.Get<string>(0));
                                    sink.Capacity = signal.Get<int>(1);
                                    StartDataCommunication(sink, ip);
                                }
                                else // //Do not alter an active sink
                                {
                                    //sink.State = SinkState.Connected;
                                    _journal.Warn($"sink {sink.Address.ToString()} is connected already");
                                }
                            }
                            break;

                        case GridSignal.CONTINUE:
                            sink.State = SinkState.Connected;
                            _journal.Debug($"sink replied with continue");
                            break;

                        case GridSignal.IMALIVE:
                            //if (sink.State == SinkState.Stop)
                            sink.State = SinkState.Connected;
                            break;

                        case GridSignal.WAIT:
                            if (sink.State != SinkState.Stop)
                            {
                                sink.State = SinkState.Wait;
                                sink.IsListening = true;  // keep on listening
                            }
                            _journal.Debug($"sink asks to wait");
                            break;

                        case GridSignal.STOP:
                            if (sink.State == SinkState.Ready)
                            {
                                sink.State = SinkState.AwaitingConfirmation;
                                sink.IsListening = true;
                                StopDataCommunication(sink);
                            }
                            break;

                        case GridSignal.RECEIVED:
                            _journal.Debug($"sink confirms the receiving of data");
                            if (sink.State != SinkState.Start || sink.State == SinkState.Stop )
                            {
                                var clientKey = signal.Get<string>(0);
                                Commit(clientKey);

                                //are there any other data ack required?
                                //if (!_transactions.Any(x => x.address.Equals(sink.Address)))
                                //{
                                //    sink.State = SinkState.Connected;
                                //}
                            }
                            break;
                        case GridSignal.RETRANSMIT:
                            _journal.Debug($"sink lost the data");
                            if (sink.State == SinkState.AwaitingConfirmation)
                            {
                                var clientKey = signal.Get<string>(0);
                                Rollback(clientKey);
                                sink.State = SinkState.Connected;
                            }
                            break;
                        case GridSignal.GOODBYE:
                            if (sink.IsActive)
                            {
                                StopDataCommunication(sink);
                                Rollback(sink.Address);
                            }
                            sink.State = SinkState.Stop;
                            _journal.Debug($"{sink.Address} stops");
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                _journal.Error(ex, $"Receiving fails for {sink.Address.ToString()}");
            }
        }




        private void StartDataCommunication(SinkControlBlock sink, IPEndPoint ip)
        {
            sink.DataChannel = new NetworkDataChannel<QuickBinarySerializer>(_host, ip);
            sink.IsActive = true;
            sink.IsListening = true;

            _journal.Debug($"Opened data channel with {sink.Address.ToString()}");
        }
        private void StopDataCommunication(SinkControlBlock sink)
        {
            if (sink.IsActive)
            {
                sink.DataChannel.Dispose();
                sink.DataChannel = null;
                sink.IsActive = false;
            }
            _journal.Debug($"Closed data channel with {sink.Address.ToString()}");
        }
        private void SendData(SinkControlBlock sink)
        {
            var key = Guid.NewGuid().ToString();
            Contract.Assert(!_transactions.Any(x => x.key == key), "duplicate data key");

            var size = Math.Min(_policy.MaxObjectsToSend, sink.Capacity);
            var objects = _buffer.Read(key, size);
            if (objects.Any())
                sink.DataChannel.SendData(key, objects);

            _transactions.Add(new DataTransaction(sink.Address, key, DateTime.UtcNow));
        }
        private void Commit(string clientKey)
        {
            if (_transactions.Any(x => x.key == clientKey))
            {
                var it = _transactions.First(x => x.key == clientKey);
                _buffer.Commit(it.key);
                _transactions.Remove(it);
            }
            else
            {
                _journal.Warn($"invalid key for data transaction commit received {clientKey}, has {string.Join(", ", _transactions.Select(y => y.key).ToArray())}");
            }
        }
        private void Rollback(string key) {
            if(_transactions.Count(x=>x.key==key)==1){
                var it = _transactions.FirstOrDefault(x => x.key == key);
                _buffer.Rollback(it.key);
                _transactions.Remove(it);
            }
        }
        private void Rollback(IPEndPoint address)
        {
            var dead = _transactions.Where(x => x.address.Equals(address));
            if (dead.Any())
            {
                _journal.Debug($"rolling back {dead.Count()} data transaction(s) sent to {address.ToString()}");
                foreach (var it in dead)
                {
                    _buffer.Rollback(it.key);
                }
            }

        }
        private void RollbackTimedOut()
        {
            var deadTransactions = _transactions.Where(x => _policy.IsTimedOut(x.sentAt));
            foreach (var it in deadTransactions)
            {
                _buffer.Rollback(it.key);
            }
        }
    }
}
