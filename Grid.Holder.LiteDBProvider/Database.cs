﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using Grid.Common;
using Grid.Common.Storage;
using LiteDB;
using LiteDB_V6;

namespace Grid.Holder.Providers.liteDb
{
    public class LiteDBProvider: IHolderStorageProvider
    {
        LiteDatabase _db;
        LiteCollection<LiteDbMessage> _inbox, _outbox, _trash;
        readonly IJournal _journal;

        public LiteDBProvider(IJournal arg)
        {
            Contract.Requires(arg != null);

            _db = null;
            _inbox = _outbox = _trash = null;
            _journal = arg;
        }
        private LiteDbMessage Pack(LiteCollection<LiteDbMessage> coll, Object msg)
        {
            var index = coll.LongCount() + 1;
            var data = Grid.Common.Utility.Serialize(msg);

            return MakeMessage(index, msg.GetType().FullName, data);
        }
        private IEnumerable<LiteDbMessage> PackMany(LiteCollection<LiteDbMessage> coll, Object[] msgs)
        {
            var list = new List<LiteDbMessage>(msgs.Length);
            var start = coll.LongCount() + 1;

            for (var index = 0; index < msgs.Length; index++)
            {
                var msg = msgs[index];
                var data = Grid.Common.Utility.Serialize(msg);
                var message = MakeMessage(start + index, msg.GetType().FullName, data);
                list.Add(message);
            }
            return list;
        }

        LiteDbMessage MakeMessage(long id, string typeName, byte[] data)
        {
            return new LiteDbMessage
            {
                Id = id,
                IsDeleted = false,
                AddedOn = DateTime.UtcNow,
                TypeName = typeName,
                Length = data.Length,
                Message = data
            };
        }
        private Object Unpack(LiteDbMessage mail)
        {
            Object msg = Utility.Deserialize<Object>(mail.Message);
            return msg;
        }
        private IEnumerable<Object> UnpackMany(IEnumerable<LiteDbMessage> messages)
        {
            var list = new List<Object>(messages.Count());

            foreach(var msg in messages)
            {
                try
                {
                    var obj = Utility.Deserialize<Object>(msg.Message);

                    if (obj != null)
                        list.Add(obj);
                }
                catch (Exception ex)
                {
                    _journal.Error(ex, "Problem in deserializing object");
                }
            }

            return list;
        }

        private LiteDB.Query GetUnprocessedItemsQuery()
        {
            var state = LiteDB.Query.EQ("State", HolderMessageState.UnProcessed.ToString());
            var notDeleted = LiteDB.Query.EQ("IsDeleted", false);
            var where = LiteDB.Query.And(state, notDeleted);
            var orderby = LiteDB.Query.All("AddedOn", LiteDB.Query.Ascending);
            var query = LiteDB.Query.And(where, orderby);

            return query;
        }
        private int CountUnprocessedObjects(LiteCollection<LiteDbMessage> collection)
        {
            return collection.Count(GetUnprocessedItemsQuery());
        }
        private Object[] GetAvailableObjects(LiteCollection<LiteDbMessage> collection, int count)
        {
            var query = GetUnprocessedItemsQuery();

            count = Math.Min(collection.Count(query), count);
            var messages = collection.Find(query, limit: count);
            var ids = messages.Select(x => x.Id).ToList();
            var objects = UnpackMany(messages);
            DeleteMessages(collection, messages);

            return objects.ToArray();
        }

        private void DeleteMessages(LiteCollection<LiteDbMessage> collection, IEnumerable<LiteDbMessage> messages)
        {

            try
            {
                var list = messages.ToList();
                list.ForEach(x => x.IsDeleted = true);
                collection.Update(list);
            }
            catch (Exception ex)
            {
                _journal.Error(ex, "Error while performing delete in LiteDb");
            }
        }

        private void EmptyQueue(LiteCollection<LiteDbMessage> collection)
        {
            collection.Delete(Query.All());
        }
        private void CreateOrOpen(string path)
        {
            _journal.Info($"Creating the database file {path}");

            _db = new LiteDB.LiteDatabase(path);

            _inbox = _db.GetCollection<LiteDbMessage>("Inbox");
            _outbox = _db.GetCollection<LiteDbMessage>("Outbox");
            _trash = _db.GetCollection<LiteDbMessage>("Trash");

            Action<LiteDB.LiteCollection<LiteDbMessage>> PrepareIndex = (arg) =>
            {
                arg.EnsureIndex(x => x.Id, true);
                arg.EnsureIndex(x => x.AddedOn);
            };

            PrepareIndex(_inbox);
            PrepareIndex(_outbox);
            PrepareIndex(_trash);
        }



        #region IGridStorage
        void IHolderStorageProvider.Initialize(FileInfo file) => CreateOrOpen(file.FullName);
        public void Dispose()
        {
            if (_db != null)
            {
                _db.Dispose();
                _db = null;
            }
        }
        void IHolderStorageProvider.Clear()
        {
            EmptyQueue(_inbox);
            EmptyQueue(_outbox);
        }


        bool IHolderStorageProvider.IsInboxEmpty => CountUnprocessedObjects(_inbox)  == 0;
        bool IHolderStorageProvider.IsOutboxEmpty => CountUnprocessedObjects( _outbox) == 0;


        Object[] IHolderStorageProvider.GetInboxMessage(int count)
        {
            return GetAvailableObjects(_inbox, count);
        }
        Object[] IHolderStorageProvider.GetOutboxMessage(int count)
        {
            return GetAvailableObjects(_outbox, count);
        }

        int IHolderStorageProvider.InboxCount => _inbox.Count();
        int IHolderStorageProvider.OutboxCount => _outbox.Count();


        void IHolderStorageProvider.PurgeInbox() => EmptyQueue(_inbox);
        void IHolderStorageProvider.PurgeOutbox() => EmptyQueue(_outbox);
        void IHolderStorageProvider.PurgeTrash() => EmptyQueue(_trash);

        void IHolderStorageProvider.SaveToInbox(IEnumerable<Object> objects)
        {
            var messages = PackMany(_inbox, objects.ToArray());
            _inbox.Insert(messages);
        }
        void IHolderStorageProvider.SaveToOutbox(IEnumerable<Object> objects)
        {
            var messages = PackMany(_outbox, objects.ToArray());
            _outbox.Insert(messages);
        }
        void IHolderStorageProvider.DeleteMessage(Object obj)
        {
            var message = Pack(_trash, obj);
            _trash.Insert(message);
        }

        #endregion
    }
}
