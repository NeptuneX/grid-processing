﻿using System;
using LiteDB;

namespace Grid.Holder.Providers.liteDb
{
    public enum HolderMessageState
    {
        UnProcessed = 0, InProcesssing, Processed
    }

    public class LiteDbMessage
    {
        [BsonId(true)]
        public long Id { get; set; }

        public bool IsDeleted { get; set; }
        public string TypeName { get; set; }

        public HolderMessageState State { get; set; }

        public DateTime AddedOn { get; set; }

        public int Length { get; set; }

        public byte[] Message { get; set; }
    }
}
