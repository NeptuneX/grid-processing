using System;

using System.Diagnostics.Contracts;
using System.IO;

using Grid.Common;
using Grid.Common.Storage;

namespace Grid.Holder
{
    public class GridHolder: 
        IHolderMailboxContract<Object>,
        IDisposable
    {
        IHolderStorageProvider _store;
        readonly  IJournal _journal;
        Tuple<IHolderMailBox<Object>, IHolderMailBox<Object> > _mailboxes;


        const int K_Inbox = 1;
        const int K_Outbox = 2;
        const string K_Storage = "MessageBox";

        public GridHolder(IHolderStorageProvider arg, IJournal journal)
        {
            Contract.Requires(arg != null);
            Contract.Requires(journal != null);

            _store = arg;
            _journal = journal;

           
        }

		public void Initialize(string name= K_Storage)
		{
            Contract.Requires(name != null && !string.IsNullOrEmpty(name.Trim()));

            var filename = Path.Combine(Utility.GetTemporaryPath(), $"{name}.cache");
            var fileinfo = new FileInfo(filename);

            _store.Initialize(fileinfo);
            _mailboxes = Tuple.Create(
                 new GridMailBox<Object>(this, K_Inbox) as IHolderMailBox<Object>,
                 new GridMailBox<Object>(this, K_Outbox) as IHolderMailBox<Object>
                );

            _journal.Info("Holder has initialized");
		}
        public  void Dispose()
        {
            if (_store != null)
            {
                _store.Dispose();
                _store = null;
            }

            _journal.Info("Holder has been disposed"); 
        }

        void IHolderMailboxContract<Object>.Enqueue(int token, Object arg)
        {
            if (token == K_Inbox)
                _store.SaveToInbox(arg);
            else
                _store.SaveToOutbox(arg);
        }
            
        Object IHolderMailboxContract<Object>.Dequeue(int token) => token == K_Inbox ? _store.GetInboxMessage() : _store.GetOutboxMessage();
        bool IHolderMailboxContract<Object>.IsEmpty(int token) => token == K_Inbox ? _store.IsInboxEmpty : _store.IsOutboxEmpty;

        public IHolderMailBox<Object> Inbox => _mailboxes.Item1;
		public IHolderMailBox<Object> Outbox => _mailboxes.Item2;
	}
}
