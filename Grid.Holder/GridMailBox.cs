﻿using Grid.Common;

namespace Grid.Holder
{
    public class GridMailBox<T> : IHolderMailBox<T>
    {
        readonly IHolderMailboxContract<T> _holder;
        readonly int _token;
 
        internal GridMailBox(IHolderMailboxContract<T> arg, int token)
        {
            _holder = arg;
            _token = token;
        }

        public void Push(T msg) => _holder.Enqueue(_token, msg);
        public T Pop() => _holder.Dequeue(_token);
        public bool IsEmpty => _holder.IsEmpty(_token);
    }
}