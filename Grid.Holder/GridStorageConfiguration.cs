﻿using System.Diagnostics.Contracts;
using System.IO;


namespace Grid.Holder
{
    public class GridStorageConfiguration
    {
        readonly FileInfo _file;
        public FileInfo File => _file;
        

        public GridStorageConfiguration(string path, string name)
        {
            Contract.Requires(!string.IsNullOrEmpty(path));
            Contract.Requires(!string.IsNullOrEmpty(name));

            _file = new FileInfo(Path.Combine(path, name));
        }
    }
}