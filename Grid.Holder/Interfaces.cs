using System;
using System.IO;

namespace Grid.Holder
{
    public interface IHolderMailboxWriter<T>
	{
		void Push(T arg);
	}
    public interface IHolderMailboxReader<T>
    {
        T Pop();
    }
    public interface IHolderMailBox<T> : IHolderMailboxReader<T>, IHolderMailboxWriter<T>
    {
        bool IsEmpty { get; }
    }

    public interface IHolderMailboxContract<T>
    {
        void Enqueue(int token, T arg);
        T Dequeue(int token);
        bool IsEmpty(int token);
    }
    
}
