﻿using System;
using System.IO;
using System.Reflection;

namespace Grid.Node
{
    class AssemblyLoader : MarshalByRefObject
    {
        public Assembly GetAssembly(string assemblyPath)
        {
            var assembly = (Assembly) null;
            try
            {
                if (File.Exists(assemblyPath))
                    assembly = Assembly.LoadFrom(assemblyPath);
                else
                    throw new FileNotFoundException($"could not find {assemblyPath}");
            }
            catch (Exception ex)
            {
                throw;
            }
            return assembly;
        }
    }

}
