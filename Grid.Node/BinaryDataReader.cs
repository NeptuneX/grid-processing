﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Grid.Node
{
    public class DictionaryDataReader : IDataReader
    {
        private List<Dictionary<string, object>> dataDictionary;
        private List<string> ColumnNames;
        private int _currentIndex = -1;
        private int _fieldCount;

        public DictionaryDataReader(List<Dictionary<string, object>> data)
        {
            this.dataDictionary = data;
            this.ColumnNames = data[0].Keys.ToList();
            this._fieldCount = this.ColumnNames.Count;
        }

        public void Close()
        {
            throw new NotImplementedException();
        }

        public int Depth
        {
            get { throw new NotImplementedException(); }
        }

        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        public bool IsClosed
        {
            get { throw new NotImplementedException(); }
        }

        public bool NextResult()
        {
            throw new NotImplementedException();
        }

        public bool Read()
        {
            if ((_currentIndex + 1) < this.dataDictionary.Count)
            {
                _currentIndex++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public int RecordsAffected
        {
            get { throw new NotImplementedException(); }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int FieldCount
        {
            get { return this._fieldCount; }
        }

        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public int GetInt32(int i)
        {
            throw new NotImplementedException();
        }

        public long GetInt64(int i)
        {
            return Convert.ToInt64(GetValue(i));
        }

        public string GetName(int i)
        {
            return this.ColumnNames[i];
        }

        public int GetOrdinal(string name)
        {
            return this.ColumnNames.IndexOf(name);
        }

        public string GetString(int i)
        {
            throw new NotImplementedException();
        }

        public object GetValue(int i)
        {
            string itemName = GetName(i);
            object value = this.dataDictionary[_currentIndex][itemName];

            //if (value != null && value.ToString() == "True")
            //    value = true;
            //else if (value != null && value.ToString() == "False")
            //    value = false;

            //if (itemName == "KeyBusinessListing" || itemName == "KeyBusinessListingContact")
            //{
            //    value = Convert.ToInt64(value);
            //}
            //else
            //{
            //    value = this.dataDictionary[_currentIndex][itemName];
            //}

            return value;
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public bool IsDBNull(int i)
        {
            throw new NotImplementedException();
        }

        public object this[string name]
        {
            get { throw new NotImplementedException(); }
        }

        public object this[int i]
        {
            get { throw new NotImplementedException(); }
        }
    }

}
