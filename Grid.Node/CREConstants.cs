﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node
{
    public static class CREConstants
    {
        public const char Delimiter = '|';
        public const string CountyFIPSDBColumn = "CountyFIPS";
        public const string CREPropertyAPNSeqDB = "CREPropertyAPNSeq";
        public const string CREPropertyAPNFormattedDB = "CREPropertyAPNFormatted";
        //note this is APN unformatted
        public const string CREPropertyAPNDB = "CREPropertyAPN";
        public const string CREPropertyStateDB = "CREPropertyState";
        public const string CREMortgagePositionDB = "CREMortgagePosition";
        public const string CRETransactionPriceDB = "CRETransactionPrice";
        public const string CRETransactionDateDB = "CRETransactionDate";
        public const string CRETransactionYearDB = "CRETransactionYear";
        public const string CRETransactionMonthDB = "CRETransactionMonth";
        public const string CREMortgageInstrNumberDB = "CREMortgageInstrNumber";
        public const string CREMortgageTermCodeDB = "CREMortgageTermCode";
        public const string CREMultiAPNCountDB = "CREMultiAPNCount";
        public const string CRECensusTractDB = "CensusTract";
        public const string MailZipCode4DB = "CREMailingZip4";
        public const string MailZipCodeDB = "CREMailingZip";
        public const string PropertyZipCode4DB = "CREPropertyZip4";
        public const string PropertyZipCodeDB = "CREPropertyZip";

        public const string CREPropertyValueBestDB = "CREPropertyValueBest";
        public const string CREPropertyValueAssessedDB = "CREPropertyValueAssessed";
        public const string CREPropertyValueImprovementDB = "CREPropertyValueImprovement";
        public const string CREPropertyValueAppraisedDB = "CREPropertyValueAppraised";
        public const string CREPropertyValueLandDB = "CREPropertyValueLand";
        public const string CREPropertyValueMarketDB = "CREPropertyValueMarket";
        public const string CREPropertyAssessedYear = "CREPropertyAssessedYear";

        public const string MortgageRecordingInstrumentNumberDB = "CREMortgageInstrNumber";
        public const string MortgageRecordingDateDB = "CREMortgageRecDate";
        public const string MortgageRecordingBatchDateDB = "CREMortgageRecBatchDate";
        public const string MortgageRecordingBatchSequenceDB = "CREMortgageRecBatchSeq";
        public const string TransactionInstrumentNumberDB = "CRETransactionInstrNumber";
        public const string TransactionRecordingDateDB = "CRETransactionRecDate";
        public const string TransactionRecordingBatchDateDB = "CRETransactionRecBatchDate";
        public const string TransactionRecordingBatchSequenceDB = "CRETransactionRecBatchSeq";
        public const string FirstPositionFile = "1ST";
        public const string SecondPositionFile = "2ND";
        public const string FirstPositionDB = "FIRST";
        public const string SecondPositionDB = "SECOND";
        public const string FirstPositionMortgage = "FIRST_POSITION";
        public const string SecondPositionMortgage = "SECOND_POSITION";
        public const string FirstMortgageTermCode = "FIRST_POSITION_MORTGAGE_TERM_CODE";
        public const string SecondMortgageTermCode = "SECOND_POSITION_MORTGAGE_TERM_CODE";
        public const string PropertyFipsCode = "FIPS_CODE";
        public const string PropertyAPNSeq = "APN_SEQUENCE_NUMBER";
        public const string PropertyAPNUnFormatted = "APN_UNFORMATTED";
        public const string TransactionRecordingDate = "CURRENT_SALE_RECORDING_DATE";
        public const string TransactionBatchDate = "CURRENT_TRANSACTION_BATCH_DATE";
        public const string FirstMorgRecordingDate = "FIRST_POSITION_MORTGAGE_RECORDING_DATE";
        public const string FirstMorgRecordingBatchDate = "FIRST_POSITION_MORTGAGE_RECORDING_BATCH_DATE";
        public const string SecondMorgRecordingDate = "SECOND_POSITION_MORTGAGE_RECORDING_DATE";
        public const string SecondMorgRecordingBatchDate = "SECOND_POSITION_MORTGAGE_RECORDING_BATCH_DATE";
        public const string FirstMortgageAddChangeFlagFile = "FIRST_POSITION_MORTGAGE_RECORDING_ADD_CHANGE_DELETE_FLAG";
        public const string SecondMortgageAddChangeFlagFile = "SECOND_POSITION_MORTGAGE_RECORDING_ADD_CHANGE_DELETE_FLAG";
        public const string PropertyAddChangeFile = "ADD_CHANGE_FLAG";
        public const string ROWNUMBER = "ROWNUMBER";
        public const string FirstOwnerDB = "OWNER_1";
        public const string SecondOwnerDB = "OWNER_2";
        //there is no consistent way to identify columns in OWNER 3 category, so...
        public static List<string> ThirdOwnerDB = new List<string>() { "OWNER_CARE_OF_NAME" };
        public static List<string> ColumnsToIgnore = null;
        public static List<string> AddChangeFlags = new List<string>() { PropertyAddChangeFile, FirstMortgageAddChangeFlagFile, SecondMortgageAddChangeFlagFile };
        public static List<string> FirstMortgagePrimaryKeys = new List<string>() { PropertyFipsCode, "FIRST_POSITION_MORTGAGE_RECORDING_INSTRUMENT_NUMBER", FirstMorgRecordingDate };
        public static List<string> FirstMortgageCandidateKeys = new List<string>() { PropertyFipsCode, FirstMorgRecordingBatchDate, "FIRST_POSITION_MORTGAGE_RECORDING_BATCH_SEQ" };
        public static List<string> SecondMortgagePrimaryKeys = new List<string>() { PropertyFipsCode, "SECOND_POSITION_MORTGAGE_RECORDING_INSTRUMENT_NUMBER", SecondMorgRecordingDate };
        public static List<string> SecondMortgageCandidateKeys = new List<string>() { PropertyFipsCode, SecondMorgRecordingBatchDate, "SECOND_POSITION_MORTGAGE_RECORDING_BATCH_SEQ" };
        public static List<string> TransactionPrimaryKeys = new List<string>() { PropertyFipsCode, TransactionRecordingDate, "SALE_INSTRUMENT_NUMBER" };
        public static List<string> TransactionSecondaryKeys = new List<string>() { PropertyFipsCode, TransactionBatchDate, "CURRENT_TRANSACTION_BATCH_SEQUENCE" };


        public const string SQLAuditConnectionDev = "SERVER=SNLSQLDEV;DATABASE=Audit;UID=systrans;PWD=pancake99";
        public const string SQLAuditConnectionProd = "SERVER=SNLDB2;DATABASE=Audit;UID=systrans;PWD=nan4tucket";
        public const string SQLInternalSystemsConnectionDev = "SERVER=SNLSQLDEV;DATABASE=InternalSystems;UID=systrans;PWD=pancake99";
        public const string SQLInternalSystemsConnectionProd = "SERVER=SNLDB2;DATABASE=InternalSystems;UID=systrans;PWD=nan4tucket";
        public enum CREKeyObject
        {
            Property = 4945,
            Mortgage = 4934,
            PropMortgage = 4997,
            Transaction = 4948,
            Owner = 5294
        }

        public struct ADDCHANGEFLAG
        {
            public const string Add = "A";
            public const string Update = "C";
            public const string Delete = "D";
        }

        public struct CREPrimaryKeyItem
        {
            public const string KeyCorpREProperty = "KeyCorpREProperty";
            public const string KeyCorpREPropMortgage = "KeyCorpREPropMortgage";
            public const string KeyCorpREMortgage = "KeyCorpREMortgage";
            public const string KeyCorpRETransaction = "KeyCorpRETransaction";
            public const string KeyCorpREOwnerName = "KeyCorpREOwnerName";
            public const string KeyCorpREPropertyOwner = "KeyCorpREPropertyOwner";
        }

        public struct AddressMatchQueue
        {
            public const string CREPropertyState = "CREPropertyState";
            public const string CREMailingState = "CREMailingState";
        }

        public enum CREAuditTables
        {
            PROPERTIES,
            TRANSACTIONUID1,
            TRANSACTIONUID2,
            MORTGAGE1UID1,
            MORTGAGE1UID2,
            MORTGAGE2UID1,
            MORTGAGE2UID2,
            OWNER1,
            OWNER2,
            OWNER3,
            PROPERTYOWNER
        }

        public enum MortgagePosition
        {
            First,
            Second
        }

        static CREConstants()
        {
            ColumnsToIgnore = new List<string>() { "MaskCREDeedSecondaryType", "MaskCREMortgageSecondaryType" 
                //"CREMultiAPNCount", 
                //"CREMailingZip4", 
                //"CREAbsenteeOwner" 
            };
        }
    }

}
