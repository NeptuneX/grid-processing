﻿using Grid.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;

namespace Grid.Node
{
    public class CREFileReader
    {
        #region Properties

        private Dictionary<string, int> FileHeaderDictionary;
        private FileInfo FileToLoad;
        private bool LoadAllAsInserts { get; set; }

        private IJournal _journal;
        #endregion

        #region Constructor

        public CREFileReader(FileInfo file,IJournal journal)
        {

            Contract.Requires(file != null && file.Exists);
            Contract.Requires(journal != null);

            bool loadAllAsInserts;

            FileToLoad = file;
            _journal = journal;

            if (bool.TryParse(ConfigurationManager.AppSettings["CRE_LoadAllAsInserts"], out loadAllAsInserts))
            {
                this.LoadAllAsInserts = loadAllAsInserts;
            }
        }

        #endregion

        #region Methods

        public Dictionary<CREConstants.CREAuditTables, TableInfo> BulkLoadRawCREFile(ref int rowNumber)
        {
            Contract.Requires(FileToLoad != null && FileToLoad.Exists,$"File {FileToLoad.FullName} is not found.");
            Contract.Requires(_journal != null);

            Contract.Ensures(Contract.Result<Dictionary<CREConstants.CREAuditTables, TableInfo>>() != null);

            Dictionary<CREConstants.CREAuditTables, TableInfo> tableColumns;
            StreamReader fileReader = null;

            try
            {
                _journal.Info("Starting load of CRE Property data file to Temp sql table...");

                try
                {

                    fileReader = new StreamReader(this.FileToLoad.FullName);
                }
                catch (Exception)
                {
                    throw new Exception("File could not be opened for processing");
                }

                //Get Headers
                var creDataHeaders = fileReader.ReadLine();

                Contract.Assert(creDataHeaders != null, $"Unable to read header line in file {FileToLoad.FullName}.");
               
                this.UpdateHeaders(creDataHeaders);
                tableColumns = this.GetTableColumns(this.FileToLoad.Name.Replace(this.FileToLoad.Extension, ""));

                CREFileParser parser = new CREFileParser(tableColumns, this.FileHeaderDictionary);
                CRERawFileDataManager rawFileDataManager = new CRERawFileDataManager(tableColumns, this.FileHeaderDictionary);

                //Create Audit Tables
                rawFileDataManager.CreateTables();

                //Load Audit Tables from CRE Raw File
                List<string> chunkContents = new List<string>();
                string line = string.Empty;
                int processedChunkCount = 1;
                bool pfJobCreated = false;

                while ((line = fileReader.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        chunkContents.Add(line);

                        if (chunkContents.Count == 50000)
                        {
                            Logger.LogEntry(LogVerbosityLevel.Information, "\r\nMapping " + chunkContents.Count + " records...");
                            var fileChunkCollection = parser.ParseCREFile(chunkContents, ref rowNumber);

                            //Load chunk
                            Logger.LogEntry(LogVerbosityLevel.Information, "Writing records to database...");

                            rawFileDataManager.InsertIntoSplitAuditTables(fileChunkCollection, ref pfJobCreated, zipFileName);

                            chunkContents.Clear();

                            Logger.LogEntry(LogVerbosityLevel.Information, "\r\nProcessed chunk count: " + processedChunkCount);
                            processedChunkCount++;
                        }
                    }
                }

                if (chunkContents.Count > 0)
                {
                    Logger.LogEntry(LogVerbosityLevel.Information, "\r\nMapping " + chunkContents.Count + " records...");
                    var fileChunkCollection = parser.ParseCREFile(chunkContents, ref rowNumber);

                    //Load chunk
                    Logger.LogEntry(LogVerbosityLevel.Information, "Writing records to database...");

                    rawFileDataManager.InsertIntoSplitAuditTables(fileChunkCollection, ref pfJobCreated, zipFileName);

                    chunkContents.Clear();
                }

                rawFileDataManager.AssignOIDsToAudit(rowNumber);

                stopwatch.Stop();
                string elapsedTime = string.Format("Elapsed time: {0}hh {1}mm {2}ss", stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);

                Logger.LogEntry(LogVerbosityLevel.Information, "Processing complete!");
                Logger.LogEntry(LogVerbosityLevel.Information, elapsedTime);
            }

            catch (Exception ex)
            {
                if (ex is CREPathFinderException)
                    (ex as CREPathFinderException).Alert();

                Logger.LogEntry(LogVerbosityLevel.Error, "Error loading file: " + ex.Message);
                throw;
            }
            finally
            {
                fileReader.Close();
            }

            return tableColumns;
        }

        /// <summary>
        /// Verifies the file is readable.
        /// </summary>
        private void VerifyFileisReadable(string file)
        {
            try
            {
                if (!File.Exists(file))  //SZ: check if the file exists or not without opening it.
                    throw new CREPathFinderException(string.Format("File {0} does not exist or incorrect path was provided.", file));

                new FileIOPermission(FileIOPermissionAccess.Read, file).Demand(); //SZ: check if the file has read permissionions. again without opening the file.
            }
            catch (Exception ex)
            {
                if (ex is SecurityException)
                    throw new CREPathFinderException(string.Format("File {0} cannot be opened due to insufficient permissions.", file));
                throw;
            }
        }

        /// <summary>
        /// Populate the headers and their index in FileHeaderDictionary.
        /// </summary>
        /// <param name="creDataHeaders">The cre data headers.</param>
        private void UpdateHeaders(string creDataHeaders)
        {
            List<string> headers = new List<string>();
            headers = creDataHeaders.Split(CREConstants.Delimiter).Select(x => x.ToUpper()).ToList();

            if (headers.Count < 2)
                throw new CREPathFinderException(string.Format("File {0} is not pipe delimited. Please check the file.", FileToLoad.FullName));

            this.FileHeaderDictionary = new Dictionary<string, int>();

            foreach (string header in headers)
            {
                string headerCleaned = header.Replace(" ", "_").Replace("-", "_").Replace(".", "_").
                    Replace(CREConstants.FirstPositionFile, CREConstants.FirstPositionDB).
                    Replace(CREConstants.SecondPositionFile, CREConstants.SecondPositionDB).ToUpper().Trim();
                this.FileHeaderDictionary.Add(headerCleaned, headers.IndexOf(header));
            }

            //SZ: Code to compare columns.
            List<string> missingColumns = new List<string>();
            var sourceList = DataManager.Mappings.Select(x => x.CREColumn).ToList<string>();
            var targetList = FileHeaderDictionary.Keys;
            foreach (var item in sourceList)
            {
                if (!targetList.Contains(item))
                    missingColumns.Add(item);
            }
            if (missingColumns.Count > 0)
                throw new CREPathFinderException("Following columns could not be found in the file: " + string.Join(",", missingColumns.ToArray()));
        }


        /// <summary>
        /// Gets the columns for for tables Prop, TUID1, TUID2, M1UID1, M2UID2..
        /// </summary>
        /// <param name="creColumns">The cre columns.</param>
        /// <param name="addChangeFlag">The add change flag.</param>
        /// <returns></returns>
        private Dictionary<string, int> GetColumns(IEnumerable<string> creColumns, string addChangeFlag = null, List<string> additionalColumns = null)
        {
            var columns = (from creColumn in creColumns.Distinct()
                           join fileHeaderColumn in this.FileHeaderDictionary on creColumn equals fileHeaderColumn.Key
                           select fileHeaderColumn).ToDictionary(c => c.Key, c => c.Value);

            if (addChangeFlag != null)
            {
                columns.Add(addChangeFlag, this.FileHeaderDictionary[addChangeFlag]);
            }
            if (additionalColumns != null)
            {
                foreach (var col in additionalColumns)
                {
                    columns.Add(col, (columns.Values.Max() + 1));
                }
            }

            return columns;
        }

        #endregion
    }

}
