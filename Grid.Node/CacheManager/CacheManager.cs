﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Grid.Common;
using Grid.Common.Process;
using Grid.Common.Storage;
using Grid.Communicator.Common;
using Grid.Holder.Providers.liteDb;
//using NetworkChannel.Common;

namespace Grid.Communicator
{

    //internal struct CacheSetting
    //{
    //    public readonly string job;
    //    public readonly IObjectBuffer receivingBuffer;
    //    public readonly IObjectBuffer sendingBuffer;
    //    public readonly IChannelWriter input;
    //    public readonly IChannelReader output;
    //    public readonly Type provider;

    //    public CacheSetting(string name, Type type, IObjectBuffer inBuffer, IObjectBuffer outBuffer, IChannelWriter inchannel, IChannelReader outChannel)
    //    {
    //        job=name;
    //        receivingBuffer=inBuffer;
    //        sendingBuffer=outBuffer;
    //         input=inchannel;
    //         output=outChannel;
    //        provider = type;
    //    }
    //}


    public class CacheManager: IDisposable
    {
        private readonly IJournal _journal;
        private readonly string _job;

        private readonly ManualResetEventSlim _signal;

        private readonly IObjectBuffer _inBuffer;
        private readonly IObjectBuffer _outBuffer;
        private readonly IChannel _inChannel;
        private readonly IChannel _outChannel;

        private readonly IHolderStorageProvider _cache;

       

        public CacheManager(
            IJournal journal, string jobName, ManualResetEventSlim signal,
            IObjectBuffer receivingBuffer, IObjectBuffer sendingBuffer, 
            IChannel input, IChannel output
            )
        {
            Contract.Requires(journal != null);
            Contract.Requires(receivingBuffer != null);
            Contract.Requires(sendingBuffer != null);
            Contract.Requires(input != null);
            Contract.Requires(output != null);

            _job = jobName;

            _journal = journal;

            _journal.Info($"Initializing grid cache {_job}");

            _signal = signal;
            _inBuffer = receivingBuffer;
            _outBuffer = sendingBuffer;

            _inChannel = input;
            _outChannel = output;

            var filename = Path.Combine(Utility.GetTemporaryPath(), $"{_job}.cache");
            var fileinfo = new FileInfo(filename);

            _cache = new LiteDBProvider(_journal);
            _cache.Initialize(fileinfo);
        }
        public bool AnythingReceived => !_inBuffer.IsEmpty;

        public bool AnythingProduced => !_outChannel.IsEmpty;

        public void Run()
        {
            while (!_signal.IsSet)
            {
                try
                {
                    if(AnythingReceived)
                        EnqueueReceivedObjects(_inBuffer);

                    
                    if (AnythingProduced)
                        EnqueForSending(_outChannel);

                    if (!_inChannel.IsFull && !_cache.IsInboxEmpty)
                    {
                        var count = _inChannel.Capacity - _inChannel.Count;
                        var objects = _cache.GetInboxMessage(count);

                        var remaining = 0;
                        if (!_inChannel.Write<object>(objects, out remaining))
                        {
                            //store the remaining objects back to inbox
                            var skip = Math.Max(0, objects.Count() - remaining);
                            _cache.SaveToInbox(objects.Skip(skip).ToArray());
                        }
                    }

                    if (!_outBuffer.IsFull && !_cache.IsOutboxEmpty)
                    {

                        var objects = _cache.GetOutboxMessage(_outBuffer.Vacancy);
                        _outBuffer.Write(objects.ToArray());
                    }

                    Thread.Sleep(250);
                    Thread.Yield();
                }
                catch (Exception ex)
                {
                    _journal.Error(ex, "problem in cache manager");
                }
            }
        }

        public void Dispose()
        {
            Contract.Ensures(_outBuffer.IsEmpty);
            Contract.Ensures(_inBuffer.IsEmpty);
            Contract.Ensures(_inChannel.IsEmpty);
            Contract.Ensures(_outChannel.IsEmpty);

            //Items that have been received or not sent out must be flushed to cache
            if (!_outBuffer.IsEmpty)
                PushTheUnsentObjects(_outBuffer);

            if (!_inBuffer.IsEmpty)
                EnqueueReceivedObjects(_inBuffer);

            //Items that have been not been processed or proc3essed but not yet sent outmust be flushed to cache
            if (!_inChannel.IsEmpty)
                SaveToInBox(_inChannel);
            if(!_outChannel.IsEmpty)
                EnqueForSending(_outChannel);
           
            _cache.Dispose();
        }

        private void EnqueueReceivedObjects(IObjectBuffer buffer)
        {
            try
            {
                var objects = buffer.Read(buffer.Count);
                _cache.SaveToInbox(objects);
            }
            catch (Exception ex)
            {
                _journal.Error(ex, "Cannot save from buffer to inbox cache");
            }
        }
        private void SaveToInBox(IChannel channel)
        {
            try
            {
                var objects = new List<object>(channel.Count);
                while (!channel.IsEmpty)
                    objects.Add(channel.Read<object>());

                _cache.SaveToInbox(objects);
            }
            catch (Exception ex)
            {
                _journal.Error(ex, "Cannot save from channel to inbox cache");
            }
            
        }
        private void PushTheUnsentObjects(IObjectBuffer buffer)
        {
            try
            {
                var size = buffer.Count;
                var objects = buffer.Read(size);
                _cache.SaveToOutbox(objects);
            }
            catch (Exception ex)
            {
                _journal.Error(ex, "Cannot save from buffer to outbox cache");
            }
        }
        private void EnqueForSending(IChannel channel)
        {
            try
            {
                var objects = channel.Read<object>(channel.Count);
                _cache.SaveToOutbox(objects);
            }
            catch (Exception ex)
            {
                _journal.Error(ex, "Cannot save from buffer to outbox cache");
            }
        }

        private string Object2String(object obj)
        {
            var data = Utility.Serialize(obj);
            var str = Convert.ToBase64String(data);

            return str;
        }
        private object String2Object(string str)
        {
            var data = Convert.FromBase64String(str);
            var obj=  Utility.Deserialize<object>(data);
            return obj;
        }
    }
}
