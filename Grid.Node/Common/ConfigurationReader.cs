﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Grid.Node.Common
{
    public abstract class ConfigurationReader<DERIVED> where DERIVED : new()
    {
        private static readonly DERIVED _Default = new DERIVED();
        public ConfigurationReader()
        {
            Init();  // SZ: class the configuration of the derived class.
            CommandTimeout = Read<int>("SQLCommandTimeoutInSeconds", 120);
            CommandTimeout = CommandTimeout < 120 ? 120 : CommandTimeout;
            Environment = (SNL.Data.Environment)Read<int>("Environment", 2);
        }
        public static DERIVED Default
        {
            get
            {
                return _Default;
            }
        }
        protected abstract void Init();

        protected T Read<T>(string key)
        {
            return Read<T>(key, default(T));
        }
        protected T Read<T>(string key, T defaultValue)
        {
            T ret = defaultValue;
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(key))
                throw new ApplicationException(string.Format("Key {0} could not be found in the configuration file", key));
            else
            {
                var tmp = ConfigurationManager.AppSettings[key];
                ret = (T)Convert.ChangeType(tmp, typeof(T));
            }
            return ret;
        }
        protected string ReadConnection(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        public int CommandTimeout { get; private set; }
        public SNL.Data.Environment Environment { get; internal set; }
    }
}
