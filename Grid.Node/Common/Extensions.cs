﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Grid.Node.Common
{
    public static class Extensions
    {
        public static KeyValuePair<TKey, TValue> ToKeyValuePair<TKey, TValue>(this Object obj)
        {
            Contract.Requires(obj != null);

            var type = obj.GetType();
            if (type.IsGenericType && type == typeof(KeyValuePair<TKey, TValue>))
            {

                //type.GetProperty("Key").GetValue(obj, null);
                //type.GetProperty("Value").GetValue(obj, null);
                //var keyObj = (TKey)key.GetValue(obj, null);
                //var valueObj = (TValue)value.GetValue(obj, null);
                return new KeyValuePair<TKey, TValue>(
                                                        (TKey)type.GetProperty("Key").GetValue(obj, null), 
                                                        (TValue)type.GetProperty("Value").GetValue(obj, null)
                                                     );

            }
            throw new ArgumentException($"obj argument must be of type KeyValuePair<{typeof(TKey).FullName},{typeof(TValue).FullName}>");

        }
    }
}
