﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using SNL.Data;
using DBG = System.Diagnostics.Debug;
using Grid.Common;

namespace Grid.Node.Common
{
    public abstract class ObjectSaver<TBUS> : IDisposable where TBUS : ISNLBusCollection, new()
    {
        protected IJournal _log;
        protected static SNL.Data.Global _global = null;
        protected static SNL.Data.Context _context = null;
        static int _count = 0;
        readonly List<Tuple<int, string>> _failed;

        public ObjectSaver(IJournal log)
        {
            _log = log;
            _failed = new List<Tuple<int, string>>();

            var initialized = false;
            _log = log;
            if (_global == null)
            {
                _global = new SNL.Data.GlobalClass();
                initialized = true;
            }
            if (_context == null)
            {
                _context = new ContextClass();
                _context.Environment = NormalizeOwnerConfiguration.Default.Environment;
                _context.SetSecurityExempt(true, "ShirleyMacLaine", -1);   // I dont know who is ShirleyMacLaine but I think it does something at the back end. 
                initialized = true;
            }
            _count += 1;
            if (initialized)
            {
                AppDomain.CurrentDomain.ProcessExit += (o, a) =>
                {
                    Action<Object> Release = (x) =>
                    {
                        if (x != null)
                            Marshal.ReleaseComObject(x);
                        x = null;
                    };

                    try
                    {
                        Release(_global);
                        Release(_context);
                    }
                    catch (Exception ex)
                    {
                        DBG.WriteLine(ex.ToString());
                    }
                };
                _log.Debug("Middle tier initialized");

            }
        }

        public bool Save()
        {
            var ret = false;
            var batch = new BatchClass();
            ISNLBusCollection col = new TBUS();
            var totalRecords = GetTotalRecords();
            var nbatch = 1;
            var totalBatches = (totalRecords / NormalizeOwnerConfiguration.Default.BatchSize) +
                    ((totalRecords % NormalizeOwnerConfiguration.Default.BatchSize) > 0 ? 1 : 0);

            try
            {
                _log.Debug("Making {0} batch(es) for {1} records", totalBatches, totalRecords);
                for (var i = 0; i < totalRecords; i++)   // The name insertion failures will not be attempted
                {

                    var inst = (Instance)col.NewEx(GetKeyAt(i), -1, -1, _context.Environment);
                    GetDataAt(i, ref inst);
                    batch.AddRequest(_global.Update(inst, (int)GenericOptions.soPrepForBatch, -1));

                    if (batch.Count >= NormalizeOwnerConfiguration.Default.BatchSize)
                    {
                        try
                        {
                            // Do not use the BatchOptions.soFastLoad. it by passes the indexes and duplicates the data.
                            _global.Execute(batch, (int)BatchOptions.soBatchTransaction | (int)BatchOptions.soRaiseDetailedError);
                        }
                        catch (Exception ex)
                        {
                            _log.Warn(ex, "Error while saving batch {0}. Processing individual records...", nbatch);
                            ProcessWithoutBatch((nbatch - 1) * NormalizeOwnerConfiguration.Default.BatchSize);
                        }
                        finally
                        {
                            nbatch += 1;
                            Marshal.ReleaseComObject(batch);
                            batch = new BatchClass();
                        }
                    }
                }
                if (batch.Count > 0)
                {
                    try
                    {
                        _global.Execute(batch, (int)BatchOptions.soBatchTransaction | (int)BatchOptions.soRaiseDetailedError);
                    }
                    catch (Exception ex)
                    {
                        _log.Warn("Error while saving batch #{0}. Processing individual records...", nbatch);
                        var remainingRecords = totalRecords % NormalizeOwnerConfiguration.Default.BatchSize;
                        ProcessWithoutBatch((int)(totalRecords - remainingRecords));
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(batch);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Error while saving the batch using middle tier {0}", ex.Message);
            }
            finally
            {
                Marshal.ReleaseComObject(col);
            }
            return ret;
        }

        private void ProcessWithoutBatch(int startIdx)
        {
            DBG.Assert(startIdx >= 0);
            ISNLBusCollection col = new TBUS();

            try
            {
                for (int i = startIdx; i < NormalizeOwnerConfiguration.Default.BatchSize; i++)
                {
                    try
                    {
                        if (i < GetTotalRecords())
                        {
                            var inst = (Instance)col.NewEx(GetKeyAt(i), -1, -1, _context.Environment);
                            GetDataAt(i, ref inst);
                            _global.Update(inst, -1, -1);
                        }
                        else break;
                    }
                    catch (Exception ex)
                    {
                        _failed.Add(new Tuple<int, string>(i, ex.Message));
                    }
                }
            }
            finally
            {
                Marshal.ReleaseComObject(col);
            }
        }

        public IList<Tuple<int, string>> FailedRecords { get { return _failed.AsReadOnly(); } }

        protected abstract string GetKeyAt(int index);
        protected abstract long GetTotalRecords();
        protected abstract bool GetDataAt(int index, ref Instance inst);
        public void Dispose()
        {
            _count--;
        }
    }
}
