﻿using System;
using System.Collections.Generic;


namespace Grid.Node.Common
{
    public enum PropertyOwnerType { FirstOwner = 0, SecondOwner = 1, CareTaker = 2 };

    public class CorpREProperty
    {
        public long Index { get; internal set; }
        public long KeyCorpREProperty { get; internal set; }
        public string OwnerFirst { get; internal set; }
        public string OwnerSecond { get; internal set; }
        public string OwnerCaretaker { get; internal set; }
        public string OwnerFirstLastName { get; internal set; }
        public string OwnerFirstFirstName { get; internal set; }
        public string OwnerFirstMI { get; internal set; }
        public string OwnerFirstSuffix { get; internal set; }
        public string OwnerSecondLastName { get; internal set; }
        public string OwnerSecondFirstName { get; internal set; }
        public string OwnerSecondMI { get; internal set; }
        public string OwnerSecondSuffix { get; internal set; }

        public string Zip { get; internal set; }
    }


    public class SqlArguments : Dictionary<string, object> { }

    public class OwnerName
    {
        public Guid Key { get; internal set; }
        public long KeyCorpREProperty { get; internal set; }
        public string FullName { get; internal set; }
        public string LastName { get; internal set; }
        public string FirstName { get; internal set; }
        public string MI { get; internal set; }
        public string Suffix { get; internal set; }

        public OwnerName()
        {
            Key = Guid.Empty;
        }
        public OwnerName(long propertyKey, string fullname, string firstName = "", string lastName = "", string mi = "", string suffix = "") : this()
        {
            KeyCorpREProperty = propertyKey;
            FullName = fullname;
            LastName = firstName;
            FirstName = lastName;
            MI = mi;
            Suffix = suffix;
        }
    }

    public struct PropertyOwnerRelation
    {
        public Guid key;
        public long propertyKey;
        public Guid ownerKey;
        public PropertyOwnerType type;

        public PropertyOwnerRelation(long arg1, Guid arg2, PropertyOwnerType arg3)
        {
            key = Guid.NewGuid();
            propertyKey = arg1;
            ownerKey = arg2;
            type = arg3;
        }

        public PropertyOwnerRelation(Guid existingkey, long arg1, Guid arg2, PropertyOwnerType arg3)
        {
            key = existingkey;
            propertyKey = arg1;
            ownerKey = arg2;
            type = arg3;
        }
    }
}
