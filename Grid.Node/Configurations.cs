﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;

namespace Grid.Node
{
    //internal enum NodeType
    //{
    //    Source = 1, Sink = 5, Flow = 10
    //}



    internal struct CommunicatorConfig
    {
        //COmmunication detail
        internal readonly string name;
        //internal readonly NodeType type;
        internal readonly IPEndPoint host;
        internal readonly List<IPEndPoint> targets;

        //User Code
        //internal readonly UserCodeConfig usercode;

        [DebuggerStepThrough()]
        internal CommunicatorConfig(string nname, IPEndPoint ip, List<IPEndPoint> destinations)
        {
            name = nname.Trim();
            //type = ntype;
            host = ip;
            targets = destinations;
            //usercode = new UserCodeConfig(nclass, nassembly);
        }
    }


    internal struct UserCodeConfig
    {
        internal readonly string assembly;
        internal readonly string classname;

        [DebuggerStepThrough()]
        internal UserCodeConfig(string file, string type)
        {
            classname = type;
            assembly = file;
        }
    }
}
