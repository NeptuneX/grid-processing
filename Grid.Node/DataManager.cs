﻿using Grid.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Grid.Node
{
    public class DataManager
    {
        private SNL.Data.Environment DataEnvironment;
        private Dictionary<CREConstants.CREAuditTables, TableInfo> TableColumns { get; set; }
        private Dictionary<string, int> FileHeaderDictionary { get; set; }
        public const int RowNumberBatchSize = 10000;

        readonly IJournal _journal;

        public DataManager(Dictionary<CREConstants.CREAuditTables, TableInfo> tableColumns, Dictionary<string, int> fileHeaderDictionary, IJournal journal)
        {
            this.TableColumns = tableColumns;
            this.FileHeaderDictionary = fileHeaderDictionary;
            _journal = journal;
            string envString = ConfigurationManager.AppSettings["Environment"];

            if (envString == "2")
            {
                this.DataEnvironment = SNL.Data.Environment.soEnvDevelopment;
            }
            else
            {
                this.DataEnvironment = SNL.Data.Environment.soEnvProduction;
            }
        }

        #region Insert Into Audit

        /// <summary>
        /// Inserts the into split audit tables Prop, TUID1, TUID2, M1UID1, M2UID2.
        /// </summary>
        /// <param name="fileChunkCollection">The file chunk collection.</param>
        /// <param name="pfJobCreated">if set to <c>true</c> [pf job created].</param>
        /// <param name="zipFileName">Name of the zip file.</param>
        public void InsertIntoSplitAuditTables(Dictionary<string, List<Dictionary<string, object>>> fileChunkCollection, ref bool pfJobCreated, string zipFileName)
        {
            foreach (var mappedFileContents in fileChunkCollection)
            {
                this.BulkInsertDictionary(mappedFileContents.Value, mappedFileContents.Key, ref pfJobCreated, zipFileName);
            }
        }

        /// <summary>
        /// Bulks the insert dictionary.
        /// </summary>
        /// <param name="valuesToWrite">The values to write.</param>
        /// <param name="destinationTableName">Name of the destination table.</param>
        /// <param name="pfJobCreated">if set to <c>true</c> [pf job created].</param>
        /// <param name="fileName">Name of the file.</param>
        private void BulkInsertDictionary(List<Dictionary<string, object>> valuesToWrite, string destinationTableName, ref bool pfJobCreated, string fileName)
        {
            try
            {
                using (SqlConnection connection = GetSQLDBConnection(true))
                {
                    connection.Open();

                    if (valuesToWrite.Count > 0)
                    {
                        DictionaryDataReader creReader = new DictionaryDataReader(valuesToWrite);

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            bulkCopy.BatchSize = 5000;

                            bulkCopy.DestinationTableName = destinationTableName;

                            _journal.Info("\r\nInserting " + valuesToWrite.Count + " records to Audit.." + bulkCopy.DestinationTableName);

                            foreach (string key in valuesToWrite[0].Keys)
                            {
                                bulkCopy.ColumnMappings.Add(key, key);
                            }

                            bulkCopy.WriteToServer(creReader);
                            _journal.Info("Success!");
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                //create pathfinder job with failed to load rows into audit tables
                if (!pfJobCreated)
                {
                    //Create your job with the messageToCreate
                    string text = string.Format("Failed to load rows into audit tables | File Name: {0} | File Location: {1} | Log File Location: {2}", fileName, ConfigurationManager.AppSettings["CREPropertyLoader_DropFolder"], "");//SNL.Logging.Internal.Helper.FullLogFilePath);
                    //CREPropertyLoader.MakePathFinderJob(text, CREProcessStream.CRELoaderRawFailures);

                    pfJobCreated = true;
                }

                _journal.Error("Error! " + ex.Message);
                throw;
            }
        }

        #endregion

        #region Create Table & Schema

        /// <summary>
        /// Creates the tables Prop, TUID1, TUID2, M1UID1, M2UID2..
        /// </summary>
        public void CreateTables()
        {
            _journal.Info("Creating temporary table for data...");
            try
            {
                foreach (var tableColumn in this.TableColumns)
                {
                    TableInfo tableInfo = tableColumn.Value;
                    string tableCreateSql = GetTableCreateStatement(tableInfo.TableName, tableInfo.TableColumns.Select(c => c.Key).ToList(), tableInfo.KeyName, tableInfo.IncludeKeyColumn, tableInfo.KeyTypeCode);

                    using (SqlConnection connection = GetSQLDBConnection(true))
                    {
                        SqlCommand createTableCommand = new SqlCommand(tableCreateSql, connection);
                        createTableCommand.CommandType = CommandType.Text;

                        connection.Open();
                        createTableCommand.ExecuteNonQuery();
                        _journal.Info("Table = " + tableColumn.Key + " created successfully!");
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _journal.Error("Error creating table: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets the table create statement.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columns">The columns.</param>
        /// <param name="keyName">Name of the key.</param>
        /// <param name="includeGuid">if set to <c>true</c> [include unique identifier].</param>
        /// <returns></returns>
        private string GetTableCreateStatement(string tableName, List<string> columns, string keyName, bool includeKey = true, TypeCode typeCode = TypeCode.Object)
        {
            StringBuilder tableCreateSQL = new StringBuilder();

            tableCreateSQL.AppendLine(string.Format("CREATE TABLE {0}", tableName));
            tableCreateSQL.AppendLine(string.Format("({0} bigint,", CREConstants.ROWNUMBER));

            if (includeKey)
            {
                if (typeCode == TypeCode.Object)
                {
                    tableCreateSQL.AppendLine(string.Format("{0} uniqueidentifier,", keyName));
                }
                else if (typeCode == TypeCode.Int32)
                {
                    tableCreateSQL.AppendLine(string.Format("{0} int,", keyName));
                }
            }

            foreach (string column in columns)
            {
                tableCreateSQL.AppendLine(string.Format("{0} varchar(255){1}", column, (columns.IndexOf(column) < (columns.Count - 1) ? "," : "")));
            }

            tableCreateSQL.AppendLine(");");

            return tableCreateSQL.ToString();
        }

        #endregion

        #region Assign OIDs

        /// <summary>
        /// Assigns the oi ds to audit.
        /// </summary>
        public void AssignOIDsToAudit(int numberOfRows)
        {
            this.AssignExistingKeys(numberOfRows);
            this.FixDuplicateKeys();
            this.GenerateUniqueOwnerKeys();
        }

        public void AssignInsertedOIDsToAudit()
        {
            this.AssignNewlyInsertedPropKeys();
        }

        private void GenerateUniqueOwnerKeys()
        {
            StringBuilder query = new StringBuilder();
            query.Append(string.Format(@"UPDATE {0} 
                                SET UniqueOwner1Key = (CONCAT(CONCAT(RTRIM(OWNER_1_FULL_NAME), '|'), CONCAT(RTRIM(OWNER_1_FIRST_NAME), '|'), CONCAT(RTRIM(OWNER_1_LAST_NAME), '|'),  
								CONCAT(RTRIM(OWNER_1_MIDDLE_INITIAL), '|'), CONCAT(RTRIM(OWNER_1_SUFFIX), '|')))", this.TableColumns[CREConstants.CREAuditTables.PROPERTIES].TableName));

            query.Append(string.Format(@"; UPDATE {0} 
                                SET UniqueOwner2Key = (CONCAT(CONCAT(RTRIM(OWNER_2_FULL_NAME), '|'), CONCAT(RTRIM(OWNER_2_FIRST_NAME), '|'), CONCAT(RTRIM(OWNER_2_LAST_NAME), '|'),  
								CONCAT(RTRIM(OWNER_2_MIDDLE_INITIAL), '|'), CONCAT(RTRIM(OWNER_2_SUFFIX), '|')))", this.TableColumns[CREConstants.CREAuditTables.PROPERTIES].TableName));

            query.Append(string.Format(@"; UPDATE {0}
                                SET UniqueOwner3Key = CONCAT(CONCAT(RTRIM(OWNER_CARE_OF_NAME), '|'), CONCAT(RTRIM(OWNER_CARE_OF_NAME), '|'), '|', '|', '|')",
                                this.TableColumns[CREConstants.CREAuditTables.PROPERTIES].TableName));

            using (SqlConnection connection = GetSQLDBConnection(true))
            {
                SqlCommand updateTableCommand = new SqlCommand(query.ToString(), connection);
                updateTableCommand.CommandType = CommandType.Text;
                try
                {
                    updateTableCommand.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["CRE_PropertyLoader_PropertyQueryTimeout"]);
                }
                catch (Exception)
                {
                    updateTableCommand.CommandTimeout = 600;
                }

                connection.Open();
                updateTableCommand.ExecuteNonQuery();
                 _journal.Info("UniqueOwnerKeys created successfully!");
                connection.Close();
            }
        }

        #region Assign Existing OIDs

        /// <summary>
        /// Assigns the Keys for rows in Audit from existing calc rows. Assign New KeyCorpREProperty, KeyCorpREMortgage and KeyCorpRETransaction values in Audit for Insert rows.
        /// </summary>
        /// <param name="numberOfRows">The number of rows.</param>
        private void AssignExistingKeys(int numberOfRows)
        {
            _journal.Info("Updating KeyCorpREProperty, KeyCorpRETransaction and KeyCorREMortgage in Audit from Calcs!");

            int lowerWindowIndex = 0;
            int upperWindowIndex = numberOfRows > RowNumberBatchSize ? RowNumberBatchSize : numberOfRows;
            using (SqlConnection connection = GetSQLDBConnection(true))
            {
                connection.Open();

                while (lowerWindowIndex < upperWindowIndex)
                {
                    this.AssignExistingKeysPerBatch(connection, lowerWindowIndex, upperWindowIndex);

                    //Readjust the window
                    lowerWindowIndex = upperWindowIndex + 1;
                    int potentialUpperWindow = upperWindowIndex + RowNumberBatchSize;
                    upperWindowIndex = numberOfRows > potentialUpperWindow ? potentialUpperWindow : numberOfRows;

                    _journal.Info(string.Format("Done for RowNumbers {0} to {1}", lowerWindowIndex, upperWindowIndex));
                }

                connection.Close();
                _journal.Info("KeyCorpREProperty, KeyCorpRETransaction and KeyCorREMortgage in Audit are updated successfully from Calcs!");
            }
        }

        /// <summary>
        /// Assigns the existing keys per batch. Assigns the Keys for rows in Audit from existing calc rows.
        /// Assign New KeyCorpREProperty, KeyCorpREMortgage and KeyCorpRETransaction values in Audit for Insert rows.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="lowerWindowIndex">Index of the lower window.</param>
        /// <param name="upperWindowIndex">Index of the upper window.</param>
        private void AssignExistingKeysPerBatch(SqlConnection connection, int lowerWindowIndex, int upperWindowIndex)
        {
            StringBuilder query = new StringBuilder();

            try
            {
                //Properties
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREProperty, this.TableColumns[CREConstants.CREAuditTables.PROPERTIES].TableName,
                    "ObjectViews.dbo.CREProperty", CREConstants.PropertyAddChangeFile
                    , "AND t.APN_UNFORMATTED=s.CREPropertyAPN AND t.APN_SEQUENCE_NUMBER=s.CREPropertyAPNSeq", lowerWindowIndex, upperWindowIndex));

                //Transactions
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpRETransaction, this.TableColumns[CREConstants.CREAuditTables.TRANSACTIONUID1].TableName, "ObjectViews.dbo.CRETransaction", CREConstants.PropertyAddChangeFile, "AND t.SALE_INSTRUMENT_NUMBER=s.CRETransactionInstrNumber AND t.CURRENT_SALE_RECORDING_DATE=s.CRETransactionRecDate", lowerWindowIndex, upperWindowIndex));
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpRETransaction, this.TableColumns[CREConstants.CREAuditTables.TRANSACTIONUID2].TableName, "ObjectViews.dbo.CRETransaction", CREConstants.PropertyAddChangeFile, "AND t.CURRENT_TRANSACTION_BATCH_DATE=s.CRETransactionRecBatchDate AND t.CURRENT_TRANSACTION_BATCH_SEQUENCE=s.CRETransactionRecBatchSeq", lowerWindowIndex, upperWindowIndex));

                //Mortgages
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE1UID1].TableName, "ObjectViews.dbo.CREMortgage", CREConstants.FirstMortgageAddChangeFlagFile, "AND t.FIRST_POSITION_MORTGAGE_RECORDING_INSTRUMENT_NUMBER = s.CREMortgageInstrNumber AND t.FIRST_POSITION_MORTGAGE_RECORDING_DATE = s.CREMortgageRecDate", lowerWindowIndex, upperWindowIndex));
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE2UID1].TableName, "ObjectViews.dbo.CREMortgage", CREConstants.SecondMortgageAddChangeFlagFile, "AND t.SECOND_POSITION_MORTGAGE_RECORDING_INSTRUMENT_NUMBER = s.CREMortgageInstrNumber AND t.SECOND_POSITION_MORTGAGE_RECORDING_DATE = s.CREMortgageRecDate", lowerWindowIndex, upperWindowIndex));
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE1UID2].TableName, "ObjectViews.dbo.CREMortgage", CREConstants.FirstMortgageAddChangeFlagFile, "AND t.FIRST_POSITION_MORTGAGE_RECORDING_BATCH_DATE = s.CREMortgageRecBatchDate AND t.FIRST_POSITION_MORTGAGE_RECORDING_BATCH_SEQ = s.CREMortgageRecBatchSeq", lowerWindowIndex, upperWindowIndex));
                query.Append(this.GetAssignExistingKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE2UID2].TableName, "ObjectViews.dbo.CREMortgage", CREConstants.SecondMortgageAddChangeFlagFile, "AND t.SECOND_POSITION_MORTGAGE_RECORDING_BATCH_DATE = s.CREMortgageRecBatchDate AND t.SECOND_POSITION_MORTGAGE_RECORDING_BATCH_SEQ = s.CREMortgageRecBatchSeq", lowerWindowIndex, upperWindowIndex));

                SqlCommand createTableCommand = new SqlCommand(query.ToString(), connection);
                createTableCommand.CommandType = CommandType.Text;
                try
                {
                    createTableCommand.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["CRE_PropertyLoader_PropertyQueryTimeout"]);
                }
                catch (Exception)
                {
                    createTableCommand.CommandTimeout = 600;
                }
                createTableCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _journal.Error("Error AssignExistingKeysPerBatch: " + ex.Message);
                throw;
            }
        }

        private void AssignNewlyInsertedPropKeys()
        {
            _journal.Info("Updating KeyCorpREProperty in Audit from Calcs following Property Inserts!");
            //first count up how many rows we have
            string query = string.Empty;
            int lowerRownum = 0;
            int upperRownum = 0;
            bool success1 = false;
            bool success2 = false;

            query = (string.Format(@"SELECT 
                                MIN(Rownumber) as lowerWindow, MAX(Rownumber) as upperWindow FROM {0}", this.TableColumns[CREConstants.CREAuditTables.PROPERTIES].TableName));
            using (SqlConnection connection = GetSQLDBConnection(true))
            {
                SqlCommand selectCommand = new SqlCommand(query.ToString(), connection);
                selectCommand.CommandType = CommandType.Text;
                try
                {
                    selectCommand.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["CRE_PropertyLoader_PropertyQueryTimeout"]);
                }
                catch (Exception)
                {
                    selectCommand.CommandTimeout = 600;
                }

                connection.Open();
                using (SqlDataReader reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        success1 = Int32.TryParse(reader["lowerWindow"].ToString(), out lowerRownum);
                        success2 = Int32.TryParse(reader["upperWindow"].ToString(), out upperRownum);
                    }
                }
                if (success1 && success2)
                {
                    _journal.Info("Determined number of rows in Audit Properties raw data table.");
                }
                else
                {
                    _journal.Info("Problem determining number of rows in the Audit Properties raw data table.");
                }
                connection.Close();
            }

            //---------------------
            //These updates need to be batched, but
            //the row numbers are not necessarily sequential so we don't know how many actual rows
            //we need to update just based on row numbers we retrieve
            //So we'll track the highest possible number we might update and compare it to the running total of batch sizes
            int rownumDiff = upperRownum - lowerRownum;
            int lowerWindowIndex = lowerRownum;
            int upperWindowIndex = lowerRownum + RowNumberBatchSize;
            int rowsProcessed = 0;

            while (rowsProcessed < rownumDiff)
            {
                using (SqlConnection connection = GetSQLDBConnection(true))
                {
                    connection.Open();
                    this.AssignNewlyInsertedPropKeysPerBatch(connection, lowerWindowIndex, upperWindowIndex);
                    connection.Close();
                    _journal.Info(string.Format("Done Updating KeyCorpREProperty in Audit for RowNumbers {0} to {1}", lowerWindowIndex, upperWindowIndex));

                    //Readjust the window
                    rowsProcessed += upperWindowIndex - lowerWindowIndex;
                    lowerWindowIndex = upperWindowIndex + 1;
                    upperWindowIndex = lowerWindowIndex + RowNumberBatchSize;
                }
            }
            _journal.Info("KeyCorpREProperty in Audit is updated successfully from Calcs following Property Inserts!");
        }

        /// <summary>
        /// Assigns the existing keys per batch. Assigns the Keys for rows in Audit from existing calc rows.
        /// Assign New KeyCorpREProperty, KeyCorpREMortgage and KeyCorpRETransaction values in Audit for Insert rows.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="lowerWindowIndex">Index of the lower window.</param>
        /// <param name="upperWindowIndex">Index of the upper window.</param>
        private void AssignNewlyInsertedPropKeysPerBatch(SqlConnection connection, int lowerWindowIndex, int upperWindowIndex)
        {
            StringBuilder query = new StringBuilder();

            try
            {
                //Properties
                query.Append(this.GetAssignInsertedPropKeysQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREProperty, this.TableColumns[CREConstants.CREAuditTables.PROPERTIES].TableName,
                    "ObjectViews.dbo.CREProperty", CREConstants.PropertyAddChangeFile
                    , "AND t.APN_UNFORMATTED=s.CREPropertyAPN AND t.APN_SEQUENCE_NUMBER=s.CREPropertyAPNSeq", lowerWindowIndex, upperWindowIndex));

                SqlCommand addNewOIDCommand = new SqlCommand(query.ToString(), connection);
                addNewOIDCommand.CommandType = CommandType.Text;
                try
                {
                    addNewOIDCommand.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["CRE_PropertyLoader_PropertyQueryTimeout"]);
                }
                catch (Exception)
                {
                    addNewOIDCommand.CommandTimeout = 600;
                }
                addNewOIDCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _journal.Error("Error AssignExistingKeysPerBatch: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets the assign-existing-keys query.
        /// </summary>
        /// <param name="keyName">Name of the key.</param>
        /// <param name="auditTableName">Name of the audit table.</param>
        /// <param name="calcTableName">Name of the calculate table.</param>
        /// <param name="addChangeColumn">The add change column.</param>
        /// <param name="andCondition">The and condition.</param>
        /// <param name="lowerWindowIndex">Index of the lower window.</param>
        /// <param name="upperWindowIndex">Index of the upper window.</param>
        /// <returns></returns>
        private string GetAssignExistingKeysQuery(string keyName, string auditTableName, string calcTableName, string addChangeColumn, string andCondition, int lowerWindowIndex, int upperWindowIndex)
        {
            return string.Format("UPDATE t {0}" +
                "SET t.{1} = s.{1} {0}" +
                "	,{4} = {0}" +
                "	CASE  {0}" +
                "		WHEN {4} <> 'D' THEN 'C' {0}" +
                "		ELSE 'D' {0}" +
                "	END {0}" +
                "FROM {2} t {0}" +
                "INNER JOIN {3} s ON t.FIPS_CODE = s.CountyFIPS {5} {0}" +
                "	AND t.RowNumber between {6} and {7} {0}",
                System.Environment.NewLine, keyName, auditTableName, calcTableName, addChangeColumn, andCondition, lowerWindowIndex, upperWindowIndex);
        }

        /// <summary>
        /// Gets the assign-existing-keys query.
        /// </summary>
        /// <param name="keyName">Name of the key.</param>
        /// <param name="auditTableName">Name of the audit table.</param>
        /// <param name="calcTableName">Name of the calculate table.</param>
        /// <param name="addChangeColumn">The add change column.</param>
        /// <param name="andCondition">The and condition.</param>
        /// <param name="lowerWindowIndex">Index of the lower window.</param>
        /// <param name="upperWindowIndex">Index of the upper window.</param>
        /// <returns></returns>
        private string GetAssignInsertedPropKeysQuery(string keyName, string auditTableName, string calcTableName, string addChangeColumn,
            string andCondition, int lowerWindowIndex, int upperWindowIndex)
        {
            return string.Format("UPDATE t {0}" +
                "SET t.{1} = s.{1} {0}" +
                "FROM {2} t {0}" +
                "INNER JOIN {3} s ON t.FIPS_CODE = s.CountyFIPS {5} {0}" +
                "	AND t.RowNumber between {6} and {7} {0}" +
                "AND t.ADD_CHANGE_FLAG <> 'D' AND t.KeyCorpREProperty IS NULL",
                System.Environment.NewLine, keyName, auditTableName, calcTableName, addChangeColumn, andCondition, lowerWindowIndex, upperWindowIndex);
        }

        #endregion


        #region Fix Duplicate OIDs

        /// <summary>
        /// Fix duplicate KeyCorpREProperty, KeyCorpREMortgage and KeyCorpRETransaction values in Audit for Insert rows.
        /// When we assign Add_change_flag A for every row (except D) and assign a new GUID to every mortgage and transaction, the duplicate rows that we may get from Corelogic
        /// may end up with different OIDs. To handle this, before we load, we fix the duplicate rows by assigning the same OID.
        /// </summary>
        private void FixDuplicateKeys()
        {
            StringBuilder query = new StringBuilder();

            try
            {
                //Transactions
                //TUID1
                query.Append(this.GetFixInitialKeyQuery(CREConstants.CREPrimaryKeyItem.KeyCorpRETransaction, this.TableColumns[CREConstants.CREAuditTables.TRANSACTIONUID1].TableName, CREConstants.TransactionPrimaryKeys, CREConstants.PropertyAddChangeFile, "CURRENT_SALE_DATE DESC, CURRENT_TRANSACTION_BATCH_DATE DESC, CURRENT_TRANSACTION_BATCH_SEQUENCE DESC"));
                //TUID2 gets the OID through code, not query and they are expected to be unique.
                ////TUID2
                //query.Append(this.GetFixInitialKeyQuery(CREConstants.CREPrimaryKeyItem.KeyCorpRETransaction, this.TableColumns[CREConstants.CREAuditTables.TRANSACTIONUID2].TableName, CREConstants.TransactionSecondaryKeys, CREConstants.PropertyAddChangeFile));

                //Mortgages
                //Fix duplicates for mortgage --m1uid1
                query.Append(this.GetFixInitialKeyQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE1UID1].TableName, CREConstants.FirstMortgagePrimaryKeys
                    , CREConstants.FirstMortgageAddChangeFlagFile, "FIRST_POSITION_MORTGAGE_RECORDING_BATCH_SEQ DESC"));
                //take the keys we already made for M1s and see if any are also M2s--update those
                query.Append(this.GetFixSecondaryKeyMortgageQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE2UID1].TableName, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE1UID1].TableName
                    , CREConstants.FirstMortgagePrimaryKeys, CREConstants.FirstMortgageAddChangeFlagFile));
                //--Fix duplicates for mortgage --m2uid1
                query.Append(this.GetFixInitialKeyQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE2UID1].TableName, CREConstants.SecondMortgagePrimaryKeys
                    , CREConstants.SecondMortgageAddChangeFlagFile, "SECOND_POSITION_MORTGAGE_RECORDING_BATCH_SEQ DESC"));
                //--Fix duplicates for mortgage  --m1uid2
                query.Append(this.GetFixInitialKeyQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE1UID2].TableName, CREConstants.FirstMortgageCandidateKeys
                    , CREConstants.FirstMortgageAddChangeFlagFile, "FIRST_POSITION_MORTGAGE_RECORDING_BATCH_SEQ DESC"));
                //--take the keys we already made for M1s and see if any are also M2s--update those
                query.Append(this.GetFixSecondaryKeyMortgageQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE2UID2].TableName
                    , this.TableColumns[CREConstants.CREAuditTables.MORTGAGE1UID2].TableName, CREConstants.FirstMortgageCandidateKeys, CREConstants.FirstMortgageAddChangeFlagFile));
                //--Fix duplicates for mortgage  --m2uid2
                query.Append(this.GetFixInitialKeyQuery(CREConstants.CREPrimaryKeyItem.KeyCorpREMortgage, this.TableColumns[CREConstants.CREAuditTables.MORTGAGE2UID2].TableName
                    , CREConstants.SecondMortgageCandidateKeys, CREConstants.SecondMortgageAddChangeFlagFile, "SECOND_POSITION_MORTGAGE_RECORDING_BATCH_SEQ DESC"));


                using (SqlConnection connection = GetSQLDBConnection(true))
                {
                    SqlCommand createTableCommand = new SqlCommand(query.ToString(), connection);
                    createTableCommand.CommandType = CommandType.Text;
                    try
                    {
                        createTableCommand.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["CRE_PropertyLoader_PropertyQueryTimeout"]);
                    }
                    catch (Exception)
                    {
                        createTableCommand.CommandTimeout = 600;
                    }

                    connection.Open();
                    createTableCommand.ExecuteNonQuery();
                    _journal.Info("KeyCorpRETransactions and KeyCorREMortgages created successfully!");
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                _journal.Error("Error assigning OIDs: " + ex.Message);
                throw;
            }
        }

        private string GetFixInitialKeyQuery(string keyName, string tableName, IEnumerable<string> colNames, string addChangeColumn, string orderByClause, bool updateNullOIDs = false)
        {
            return string.Format(";with ListOfKeys as ( {0}" +
                "SELECT * FROM ( {0}" +
                "	SELECT {2}, {3},  {0}" +
                "	ROW_NUMBER() OVER ( PARTITION BY {3} {0}" +
                "	ORDER BY {0}" +
                "		{6} ) AS 'RowNum' {0}" +
                "	from {1} {0}" +
                "WHERE {5}='A'  " +
                (updateNullOIDs ? "{0}and {2} is null" : string.Empty) +
                ") {0}" +
                "	 tUnique WHERE RowNum = 1) {0}" +
                "update t {0}" +
                "set t.{2} = k.{2} {0}" +
                "from {1} t {0}" +
                "join ListOfKeys k on {4} {0}{0}",
                System.Environment.NewLine, tableName, keyName, string.Join(",", colNames),
                string.Join(" AND ", colNames.Select(c => string.Format("k.{0} = t.{0}", c))), addChangeColumn, orderByClause);
        }

        private string GetFixSecondaryKeyMortgageQuery(string keyName, string targetTableName, string sourceTableName, IEnumerable<string> colNames, string addChangeColumn)
        {
            return string.Format("update m2 {0}" +
                "SET m2.{3} = m1.{3} {0}" +
                "FROM {1} m2 {0}" +
                "Join {2} m1  on {4} " +
                "AND m1.{5}='A' {0}{0}",
                System.Environment.NewLine, targetTableName, sourceTableName, keyName, string.Join(" AND ",
                colNames.Select(c => string.Format("m2.{0} = m1.{1}", c.Replace(CREConstants.FirstPositionDB, CREConstants.SecondPositionDB), c))), addChangeColumn);
        }




        #endregion

        #endregion

        #region Connection Methods

        /// <summary>
        /// Get a SQL connection
        /// </summary>
        /// <param name="useDB2"></param>
        /// <returns></returns>
        private SqlConnection GetSQLDBConnection(bool load = false)
        {
            SqlConnection connection = null;

            try
            {
                if (!load)
                {
                    connection = new SqlConnection(ConfigurationManager.AppSettings["ObjectViewsConnectString"]);
                }
                else
                {
                    //connection = new SqlConnection(ConfigurationManager.AppSettings["AuditConnectString"]);

                    if (this.DataEnvironment == SNL.Data.Environment.soEnvDevelopment)
                    {
                        connection = new SqlConnection(CREConstants.SQLAuditConnectionDev);
                    }
                    else if (this.DataEnvironment == SNL.Data.Environment.soEnvProduction)
                    {
                        connection = new SqlConnection(CREConstants.SQLAuditConnectionProd);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return connection;
        }

        #endregion
    }
}
