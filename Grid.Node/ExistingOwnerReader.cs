﻿using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using Grid.Node.Common;
using Grid.Node.NormalizeOwner.Collectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyOS;

namespace Grid.Node
{
    class ExistingOwnerReader : TinyProcessBase
    {

        private UniqueNamesCollection _uniqueNames;
        protected override void Init()
        {
            base.Init();
            _uniqueNames = new UniqueNamesCollection(Context.Journal);
        }

        public override void Run()
        {

            while (Context.Continue)
            {
                //Context.Write("Reading");
                //var testing = Context.Read<KeyValuePair<string, long>>();
                //using (var cache = new GridHolder(new InMemoryProvider.InMemoryProvider(Context.Journal), Context.Journal))
                {
                    
                   // while (!cache.Inbox.IsEmpty)
                    {
                        var zipCode = Context.Read<KeyValuePair<string, long>>();
                        Context.Write(zipCode);


                        
                        //using (var newOwners = new UniquePropertyOwners(Context.Journal, zipCode.Key))
                        //{
                        //    Context.Journal.Debug($"Existing Owner Recieved Zip Code: {zipCode.Key}");

                        //    Context.Write(new Tuple<ExistingOwnerCollector, UniquePropertyOwners>(currentOwners, newOwners));
                        //    //Context.Write($"Existing Owner Recieved Zip Code: {zipCode.Key}");
                        //    //Context.Journal.Debug(zipCode.Key);
                        //    //cache.Inbox.Push(zipCode);
                        //    //Context.Journal.Debug($"Existing Owner Recieved Sent Zip Code: {zipCode.Key} to Validator");
                        //   // Context.Write($"Existing Owner Recieved Sent Zip Code: {zipCode.Key} to Validator");

                        //}
                        var currentOwners = new ExistingOwnerCollector(Context.Journal, zipCode.Key);
                        using (var newOwners = new UniquePropertyOwners(Context.Journal, zipCode.Key))
                        {
                            //process the first owners
                            Context.Journal.Debug("     First owners  {0:n0}", currentOwners.FirstOwners.Count);
                            foreach (var it in currentOwners.FirstOwners)
                            {
                                var ownerkey = _uniqueNames.GetKey(it);
                                if (!newOwners.Exist(it.KeyCorpREProperty, ownerkey, PropertyOwnerType.FirstOwner))
                                    newOwners.Add(it.KeyCorpREProperty, ownerkey, PropertyOwnerType.FirstOwner);
                            }

                            Context.Journal.Debug("     Second owners  {0:n0}", currentOwners.SecondOwners.Count);
                            foreach (var it in currentOwners.SecondOwners)
                            {
                                var ownerkey = _uniqueNames.GetKey(it);
                                if (!newOwners.Exist(it.KeyCorpREProperty, ownerkey, PropertyOwnerType.SecondOwner))
                                    newOwners.Add(it.KeyCorpREProperty, ownerkey, PropertyOwnerType.SecondOwner);
                            }

                            Context.Journal.Debug("     Caretaker  {0:n0}", currentOwners.CareTakers.Count);
                            foreach (var it in currentOwners.CareTakers)
                            {
                                var ownerkey = _uniqueNames.GetKey(it);
                                if (!newOwners.Exist(it.KeyCorpREProperty, ownerkey, PropertyOwnerType.CareTaker))
                                    newOwners.Add(it.KeyCorpREProperty, ownerkey, PropertyOwnerType.CareTaker);
                            }
                            var recordsAdded = Convert.ToInt64(_uniqueNames.PendingRecords + newOwners.PendingRecords);

                            Context.Write(new Tuple<UniqueNamesCollection, UniquePropertyOwners>(_uniqueNames, newOwners));
                            
                            Context.Journal.Info("Processed  {0:n0} records", recordsAdded);
                            //marker.MarkAsDone(zipCode.Key);
                        }

                    }


                }

            }
        }
    }
}
