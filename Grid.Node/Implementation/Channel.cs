﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using Grid.Communicator.Common;

namespace Grid.Common.Process
{
    public class Channel : IChannel
    {
        readonly int _capacity;
        readonly TimeSpan _delay;
        readonly ConcurrentQueue<Object> _queue;
        
        bool _isClosed;

        public Channel(int capacity)
        {
            Contract.Requires(capacity >= 1024);


            _capacity = capacity;
            _queue = new ConcurrentQueue<Object>();
            _delay = TimeSpan.FromMilliseconds(200);
            _isClosed = false;
        }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(_queue != null);
            Contract.Invariant(_queue.Count <= _capacity);
        }


        public bool IsEmpty => _queue.Count == 0;
        public bool IsFull => _queue.Count == _capacity;
        public bool IsClosed => _isClosed;
        public int Count => _queue.Count;
        public int Capacity => _capacity;

        public void Write<T>(T arg)
        {
            _queue.Enqueue(arg);
        }
        public T Read<T>()
        {
            object obj=null;

            var exit = _queue.TryDequeue(out obj);
            while (!exit)
            {
                Thread.Sleep(_delay);
                exit = _queue.TryDequeue(out obj);
            }

            T ret = typeof(T) is IConvertible ? (T) Convert.ChangeType(obj, typeof(T)) : (T) obj;
            return ret;
        }

        public void Close()
        {
            _isClosed = true;
        }

        public IEnumerable<T> Read<T>(int count) where T:class, new()
        {
            Contract.Ensures(Contract.Result<IEnumerable<T>>()!=null);

            count = Math.Min(count, _queue.Count);
            var ret = new List<T>(count);
            while (count-- > 0)
            {
                object tmp;
                if(_queue.TryDequeue(out tmp))
                {
                    ret.Add(tmp as T);
                }
            }
            return ret.Where(x=> x!=null);
        }

        public bool Write<T>(IEnumerable<T> objects, out int remaining) where T : class, new()
        {
            var ret = true;
            remaining = objects.Count();
            foreach(var obj in objects)
            {
                if (_queue.Count >= _capacity)
                {
                    ret = false;
                    break;
                }
                _queue.Enqueue(obj);
                remaining--;
            }

            return ret;
        }
    }

    //public class XChannel : IChannel
    //{
    //    DataBuffer<object> _buffer;

    //    readonly TimeSpan _delay;

    //    bool _isClosed;

    //    public XChannel(int capacity)
    //    {
    //        Contract.Requires(capacity >= 1024);


    //        //_capacity = capacity;
    //        _buffer = new DataBuffer<object>(capacity, false, false);
    //        _delay = TimeSpan.FromMilliseconds(200);
    //        _isClosed = false;
    //    }

    //    [ContractInvariantMethod]
    //    private void Invariant()
    //    {
    //        Contract.Invariant(_buffer != null);
    //        //Contract.Invariant(_buffer.Count <= _capacity);
    //    }


    //    public bool IsEmpty => _buffer.IsEmpty;
    //    public bool IsFull => _buffer.IsFull;
    //    public bool IsClosed => _isClosed;
    //    public int Count => _buffer.Count;

    //    public void Write<TData>(TData arg)
    //    {
    //        if (!_isClosed)
    //            _buffer.Write(new object[] { arg });
    //        else
    //            throw new InvalidOperationException("Channel is already closed");
    //    }
    //    public T Read<T>()
    //    {
    //        while (_buffer.IsEmpty)
    //            Thread.Sleep(_delay);

    //        var obj = _buffer.Read(1).FirstOrDefault();
    //        return (T)Convert.ChangeType(obj, typeof(T));
    //    }
    //    public void Close()
    //    {
    //        _isClosed = true;
    //    }

    //    public IObjectBuffer Buffer => _buffer;
    //}

}
