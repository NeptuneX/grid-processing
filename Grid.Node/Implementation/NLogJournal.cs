﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using NLog;

namespace Grid.Common
{
    public class NLogJournal : IJournal, IDisposable
    {
        ILogger _logger;


        public NLogJournal(string name)
        {
            LogManager.ThrowExceptions = true;
            LogManager.ThrowConfigExceptions = true;
            
            _logger = LogManager.GetLogger(name);
            _logger.Info($"The journal has been started in {(Utility.IsDebug ? "DEBUG":"RELEASE")} mode");
        }
        void IDisposable.Dispose()
        {
            LogManager.Flush();
            LogManager.Shutdown();
            _logger = null;
        }

        void IJournal.Debug(string msg, params object[] args) => _logger.Debug(msg, args);
        void IJournal.Debug(Exception ex, string msg, params object[] args) => _logger.Debug(ex, msg, args);
        void IJournal.Error(string msg, params object[] args) => _logger.Error(msg, args);
        void IJournal.Error(Exception ex, string msg, params object[] args) => _logger.Error(ex, msg, args);
        void IJournal.Info(string msg, params object[] args) => _logger.Info(msg, args);
        void IJournal.Warn(string msg, params object[] args) => _logger.Warn(msg, args);
        void IJournal.Warn(Exception ex, string msg, params object[] args) => _logger.Warn(ex, msg, args);
    }
}
