﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Grid.Common;
using Grid.Common.Process;

namespace Grid.Node.Threads
{


    internal enum ExecutionState : byte
    {
        Unknown = 0 , Load=1, Initialize=2, Run=3, Destroy=4, Terminate=5
    }

    internal class NodeContext : INodeContext
    {
        bool _fault = false;


        ExecutionState _last;
        readonly Queue<ExecutionState> _states = new Queue<ExecutionState>();



        internal NodeContext(IChannelReader inpipe, IChannelWriter outpipe, IJournal journal)
        {
            Journal = journal;

            InPipes = new IChannelReader[] { inpipe };
            OutPipes =new IChannelWriter[] { outpipe };
            _states.Enqueue(ExecutionState.Load);
        }


        public ExecutionState CurrentState
        {
            get
            {
                if (_states.Count == 0)
                    CalculateNextState();
                _last = _states.Peek();
                return _last;
            }
        }
        public void Fault(Exception ex)
        {
            Journal.Error(ex);
            _fault = true;
        }

        public void CalculateNextState()
        {
            var ret = ExecutionState.Unknown;

            if (_fault)
            {
                _states.Clear();
                if(_last > ExecutionState.Load && _last < ExecutionState.Destroy)
                    _states.Enqueue(ExecutionState.Destroy);
                else
                     _states.Enqueue(ExecutionState.Terminate);
                _fault = false;
            }
            else {
                ret = _last < ExecutionState.Terminate ? _last + 1 : ExecutionState.Terminate;
                if (_states.Count == 0)
                    _states.Enqueue(ret);
            }
        }
 
        #region INodeContext
        public IChannelReader In => InPipes[0];
       
        public IJournal Journal { get; }
        public IChannelWriter Out => OutPipes[0];


        public IChannelReader[] InPipes { get; }
        public IChannelWriter[] OutPipes  { get; }


        public void Continue()
        {
            if (_states.Count > 0)
                _states.Dequeue();

            CalculateNextState();
        }
        public void Exit()
        {
            if (_last == ExecutionState.Destroy)
                _states.Enqueue(ExecutionState.Terminate);
            else
            {
                _states.Clear();
                _states.Enqueue(ExecutionState.Destroy);
            }
        }
        public void Repeat()
        {
            if(_last == ExecutionState.Initialize || _last == ExecutionState.Run)
                _states.Enqueue(_last);
        }
        public void Restart()
        {
            if (_last == ExecutionState.Initialize || _last == ExecutionState.Run)
            {
                _states.Clear();
                _states.Enqueue(ExecutionState.Destroy);
                _states.Enqueue(ExecutionState.Initialize);
            }
            else if(_last == ExecutionState.Destroy)
            {
                _states.Clear();
                _states.Enqueue(ExecutionState.Initialize);
            }
        }
       
        public T Read<T>() 
        {
            return In.Read<T>();
        }
        public void Write<T>(T arg)
        {
            Out.Write<T>(arg);
        }
        #endregion
    }
}