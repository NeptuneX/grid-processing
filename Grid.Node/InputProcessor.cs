﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node
{
    sealed class InputProcessor
    {

        internal InputProcessor()
        {
            ShouldWait = false;
            ShouldQuit = false;
        }

        public bool IsInputAvailable { get { return Console.KeyAvailable; } }

        public bool ShouldWait { get; private set; }
        public bool ShouldQuit { get; private set; }

        internal void Run()
        {
            if (IsInputAvailable)
            {
                var input = Console.In.ReadLine().Trim().ToLower();
                switch (input)
                {
                    case "refresh": break;  //Not Implemented
                    case "pause": ShouldWait = true; break;
                    case "resume": ShouldWait = false; break;
                    case "bye": ShouldWait = false; ShouldQuit = true; break;
                }
            }
        }


    }
}
