﻿using Grid.Holder;
using Grid.Node.Common;
using Grid.Node.NormalizeOwner.Collectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyOS;

namespace Grid.Node
{
    public class LoaderNode : TinyProcessBase
    {
        public override void Run()
        {
            while (Context.Continue)
            {
                //using (var cache = new GridHolder(new InMemoryProvider.InMemoryProvider(Context.Journal), Context.Journal))
                {
                    var data = Context.Read<Tuple<UniqueNamesCollection,UniquePropertyOwners>>();

                    data.Item1.Update();
                    data.Item2.Update();
                    //var zipCode = Context.Read<KeyValuePair<string, long>>();
                    ////var zipCode = Context.Read<KeyValuePair<string, long>>();
                    Context.Journal.Debug($"Saving Record");
                    Context.Write($"Saving Record");
                    //throw new NotImplementedException();
                }
            }
        }
    }
}
