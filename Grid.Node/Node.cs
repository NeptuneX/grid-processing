using System;
using System.Linq;
using System.Threading;
using Grid.Common;
using Grid.Common.Process;
using Grid.Communicator;
using Grid.Holder;
using Grid.Holder.Providers.liteDb;


namespace Grid.Node
{
    class Node : IDisposable
    {
        readonly CommunicatorConfig _data;
        readonly ManualResetEventSlim _event;

        readonly IJournal _journal;
        readonly IChannel _in, _out;

        readonly Communicator.Communicator _network;
        readonly CacheManager _cacheManager;
        readonly UserCode _executer;

        readonly CommunicationPolicy KPolicy;


        internal Node(IJournal journal, CommunicatorConfig config, ManualResetEventSlim signal, IChannel reader, IChannel writer)
        {
            _journal = journal;

            _journal.Debug($"Node [{_data.name}] is initializing....");
            _data = config;
            _event = signal; // new ManualResetEventSlim(false);

            //_journal.Debug("Initializing cache ....");
            //_storage = new GridHolder(new LiteDBProvider(_journal), _journal);
            //_storage.Initialize(_data.name);

            //_journal.Debug("Initializing internal channels ...");
            _in = reader;
            _out =writer;

            //_code = new UserCode(_data, _journal, _event, _in, _out);
            //_code.Start();

            _journal.Debug("Initializing communication channels ...");

            KPolicy = new CommunicationPolicy(
                    messageTimeout: TimeSpan.FromSeconds(30),
                    idletime: TimeSpan.FromSeconds(80),
                    maxretries: 3,
                    maxobjects: 1000
                    );
            //_network = new Grid.Communicator.Communicator(_data.host, _journal, KPolicy);

            _journal.Debug("Initializing cache ....");
            //_cacheManager = new CacheManager(_journal, _data.name,
            //    _network.Received, _network.Sent,
            //    _in, _out
            //    );

            foreach (var it in _data.targets)
                _network.AddSink(it);


            //switch (_data.type)
            //{
            //    case NodeType.Source:
            //        _source = new DataSource(_journal, KPolicy, _out.Buffer, _data.host, _data.targets.FirstOrDefault());
            //        _sink = null;
            //        break;
            //    case NodeType.Sink:
            //        _source = null;
            //        _sink = new DataSink(_journal, KPolicy, _in.Buffer, _data.host);
            //        break;
            //    case NodeType.Flow:
            //        _source = new DataSource(_journal, KPolicy, _out.Buffer, _data.host, _data.targets.FirstOrDefault());
            //        _sink = new DataSink(_journal, KPolicy, _in.Buffer, _data.host);
            //        break;
            //}

            _journal.Debug($"Node [{_data.name}] is ready.");
        }

        internal void Run()
        {
            //var command = new InputProcessor();

            while (!_event.IsSet)
            {
                //process any user input
                //command.Run();

                //if (!command.ShouldWait)
                //{
                try
                {
                    _network.Run();
                    _cacheManager.Run();
                }
                catch (Exception ex)
                {
                    _journal.Error(ex, "Problem in Communicator");
                }

                    //_source?.Run();
                    //_sink?.Run();
               // }

                //Move items from Inbox to processing channel
                //if (!_storage.Inbox.IsEmpty)
                //{
                //    var count = 0;
                //    if(!_in.IsFull && count++<10)
                //        _in.Write<object>(_storage.Inbox.Pop());
                //}

                //Move items from processed channel to outbox
                //{
                    //var duration = new NetworkChannel.Common.Delay(TimeSpan.FromMilliseconds(500));
                    //if(!_out.IsEmpty)
                    //{
                    //    var count = 0;
                    //    while (!_out.IsEmpty && count++ < 10)
                    //        _storage.Outbox.Push(_out.Read<object>());
                    //}
                //}
               

                //User wants to quit so Quit!
                //if (command.ShouldQuit)
                //    _event.Set();
            }


        }

        public void Dispose()
        {

            _journal.Warn($"Shutting down of node {_data.name} has started...");


            _journal.Debug("closing communication channels...");
            _network.Dispose();
            //if (_source != null)
            //    _source.Dispose();
            //if (_sink != null)
            //    _sink.Dispose();

            _journal.Debug("closing storage ...");
            //_storage.Dispose();
            _cacheManager.Dispose();


            _journal.Debug("clossing data passing channels...");
            //while (!_in.IsEmpty)
            //    _storage.Inbox.Push(_in.Read<object>());
            _in.Close();

            //while (!_out.IsEmpty)
            //    _storage.Outbox.Push(_out.Read<object>());
            _out.Close();


            _event.Dispose();

            _journal.Debug($"node {_data.name} is shutdown. ...");
        }

    }
}