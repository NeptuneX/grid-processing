﻿using Grid.Common;
using Grid.Node.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Grid.Node.NormalizeOwner.Collectors
{
    public class ExistingOwnerCollector : DataCollection, IEnumerable<CorpREProperty>
    {
        HashSet<CorpREProperty> _owners;
        List<OwnerName> _firstOwners, _secondOwners, _careTakers;
        readonly string _countyFIPS;

        public ExistingOwnerCollector(IJournal log, string countyFip) : base(log)
        {
            _countyFIPS = countyFip;
            _owners = new HashSet<CorpREProperty>();
            _firstOwners = null;
            _secondOwners = null;
            _careTakers = null;
            using (var rd = GetReader(TSQL.GET_CURRENT_OWNER_INFO, new SqlArguments { { "@zip", _countyFIPS } }))
            {
                Dictionary<string, int> ordinals = new Dictionary<string, int>();
                for (int i = 0; i < rd.FieldCount; i++)
                    ordinals.Add(rd.GetName(i), i);

                while (rd.Read())
                {
                    var record = new CorpREProperty();
                    try
                    {

                        record.Index = rd.GetInt64(ordinals["rowid"]);
                        record.KeyCorpREProperty = rd.GetInt32(ordinals["KeyCorpREProperty"]);

                        record.OwnerFirst = GetString(rd, ordinals["CREPropertyOwnerFirst"]);
                        record.OwnerFirstFirstName = GetString(rd, ordinals["CREPropertyOwnerFirstFirstName"]);
                        record.OwnerFirstLastName = GetString(rd, ordinals["CREPropertyOwnerFirstLastName"]);
                        record.OwnerFirstMI = GetString(rd, ordinals["CREPropertyOwnerFirstMI"]);
                        record.OwnerFirstSuffix = GetString(rd, ordinals["CREPropertyOwnerFirstSuffix"]);

                        record.OwnerSecond = GetString(rd, ordinals["CREPropertyOwnerSecond"]);
                        record.OwnerSecondFirstName = GetString(rd, ordinals["CREPropertyOwnerSecondFrstName"]);
                        record.OwnerSecondLastName = GetString(rd, ordinals["CREPropertyOwnerSecondLastName"]);
                        record.OwnerSecondMI = GetString(rd, ordinals["CREPropertyOwnerSecondMI"]);
                        record.OwnerSecondSuffix = GetString(rd, ordinals["CREPropertyOwnerSecondSuffix"]);

                        record.OwnerCaretaker = GetString(rd, ordinals["CREPropertyOwnerCaretaker"]);
                        record.Zip = GetString(rd, ordinals["CREPropertyZip"]);

                        _owners.Add(record);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex, "Failed while processing row {0}", record.Index);
                    }
                }
                rd.Close();
            }
        }

        public IEnumerator<CorpREProperty> GetEnumerator()
        {
            return ((IEnumerable<CorpREProperty>)this._owners).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<CorpREProperty>)this._owners).GetEnumerator();
        }
        public List<OwnerName> FirstOwners
        {
            get
            {
                if (_firstOwners == null)
                    _firstOwners = _owners.Where(x => !string.IsNullOrEmpty(x.OwnerFirst)).Select(x =>
                             new OwnerName(
                                 x.KeyCorpREProperty,
                                 x.OwnerFirst,
                                 x.OwnerFirstFirstName,
                                 x.OwnerFirstLastName,
                                 x.OwnerFirstMI,
                                 x.OwnerFirstSuffix)
                            ).ToList();

                return _firstOwners;
            }
        }
        public List<OwnerName> SecondOwners
        {
            get
            {
                if (_secondOwners == null)
                    _secondOwners = _owners.Where(x => !string.IsNullOrEmpty(x.OwnerSecond)).Select(x =>
                           new OwnerName(
                               x.KeyCorpREProperty,
                               x.OwnerSecond,
                               x.OwnerSecondFirstName,
                               x.OwnerSecondLastName,
                               x.OwnerSecondMI,
                               x.OwnerSecondSuffix)
                            ).ToList();

                return _secondOwners;
            }
        }
        public List<OwnerName> CareTakers
        {
            get
            {
                if (_careTakers == null)
                    _careTakers = _owners.Where(x => !string.IsNullOrEmpty(x.OwnerCaretaker)).Select(x =>
                            new OwnerName(
                                x.KeyCorpREProperty,
                                x.OwnerCaretaker,
                                x.OwnerCaretaker,
                                string.Empty,
                                string.Empty,
                                string.Empty)
                            ).ToList();

                return _careTakers;
            }
        }

    }
}
