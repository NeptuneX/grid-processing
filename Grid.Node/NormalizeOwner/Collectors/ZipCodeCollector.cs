﻿using Grid.Common;
using Grid.Node.NormalizeOwner.Collectors;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Grid.Node.NormalizeOwner
{
    public class ZipCodeCollector : DataCollection, IEnumerable<KeyValuePair<string, long>>
    {
        List<KeyValuePair<string, long>> _states;

        internal ZipCodeCollector(IJournal log) : base(log)
        {
            _states = new List<KeyValuePair<string, long>>();
            try
            {
                using (var rd = GetReader(TSQL.GET_CODES_AND_COUNT))
                {
                    int locCountyFips = rd.GetOrdinal("CountyFIPS");
                    int locZipCount = rd.GetOrdinal("ZipCount");
                    while (rd.Read())
                        //_states.Add(new KeyValuePair<string, long>(ValueOf<string>(rd, "CountyFIPS"), ValueOf<long>(rd, "ZipCount")));
                        _states.Add(new KeyValuePair<string, long>(rd.GetString(locCountyFips), rd.GetInt32(locZipCount)));
                    rd.Close();
                }

                TotalRecords = 0L;
                _states.ForEach(x => TotalRecords += x.Value);

                _log.Debug("Retrieved {0:n0} unique CountyFIPS codes with records {1:n0}", _states.Count, TotalRecords);
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Could not get CountyFIPS");
            }
        }

        public long TotalRecords { get; private set; }

        public IEnumerator<KeyValuePair<string, long>> GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<string, long>>)this._states).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<string, long>>)this._states).GetEnumerator();
        }
    }
}
