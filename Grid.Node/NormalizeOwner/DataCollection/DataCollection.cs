﻿using Grid.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBG = System.Diagnostics.Debug;

namespace Grid.Node.NormalizeOwner.Collectors
{
    public class DataCollection : IDisposable
    {
        IDbConnection _cnn = null;
        protected IJournal _log = null;

        public DataCollection(IJournal log)
        {
            try
            {
                DBG.Assert(log != null);
                _log = log;
                _cnn = new SqlConnection(NormalizeOwnerConfiguration.Default.ConnectionString);
                _cnn.Open();
                DBG.Assert(_cnn.State == ConnectionState.Open);
            }
            catch (Exception ex)
            {
                _cnn = null;
                _log.Error(ex);
            }

        }
        public void Dispose()
        {
            if (_cnn != null)
            {
                _cnn.Close();
                _cnn.Dispose();
                _cnn = null;
            }
        }

        protected internal IDbCommand GetCommand(string sql)
        {
            var ret = _cnn.CreateCommand();
            ret.CommandText = sql;
            ret.CommandType = CommandType.Text;
            ret.CommandTimeout = NormalizeOwnerConfiguration.Default.CommandTimeout;
            return ret;
        }
        protected internal T ValueOf<T>(IDataReader rd, string column)
        {
            var ret = default(T);

            var obj = rd[column];
            if (obj != null && (!(obj is DBNull)))
            {
                try
                {
                    ret = (T)Convert.ChangeType(obj, typeof(T));
                }
                catch (Exception ex)
                {
                    _log.Error(ex, "Error while converting the data");
                }
            }
            return ret;
        }

        protected internal string GetString(IDataReader rd, int i)
        {
            var ret = string.Empty;
            if (!rd.IsDBNull(i))
                ret = rd.GetString(i);
            return ret;
        }

        protected IDataReader GetReader(string sql, Dictionary<string, object> args = null)
        {
            IDataReader rd = null;

            using (var cmd = GetCommand(sql))
            {
                if (args != null)
                {
                    foreach (var key in args.Keys)
                    {
                        var p = cmd.CreateParameter();
                        p.ParameterName = key;
                        p.Direction = ParameterDirection.Input;
                        p.Value = args[key];
                        cmd.Parameters.Add(p);
                    }
                }
                rd = cmd.ExecuteReader();
            }

            return rd;
        }
        protected T GetScalar<T>(string sql, Dictionary<string, object> args = null)
        {
            var ret = default(T);

            using (var cmd = GetCommand(sql))
            {
                if (args != null)
                {
                    foreach (var key in args.Keys)
                    {
                        var p = cmd.CreateParameter();
                        p.ParameterName = key;
                        p.Direction = ParameterDirection.Input;
                        p.Value = args[key];
                        cmd.Parameters.Add(p);
                    }
                }
                var tmp = cmd.ExecuteScalar();
                if (tmp != null && !(tmp is DBNull))
                    ret = (T)Convert.ChangeType(tmp, typeof(T));
            }

            return ret;
        }
    }
}
