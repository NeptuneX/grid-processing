﻿using Grid.Common;
using Grid.Node.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node.NormalizeOwner.Collectors
{
    public class UniqueNamesCollection : DataCollection
    {
        IDictionary<string, Guid> _names;

        List<OwnerName> _unmatachedNames;
        internal UniqueNamesCollection(IJournal log) : base(log)
        {
            _names = new Dictionary<string, Guid>();
            _unmatachedNames = new List<OwnerName>();
            try
            {
                using (var rd = GetReader(TSQL.GET_UNIQUE_NAMES))
                {
                    int locKeyOwner = rd.GetOrdinal("KeyCorpREOwnerName");
                    int locFullName = rd.GetOrdinal("CREOwnerFullName");

                    while (rd.Read())
                    {
                        var value = rd.GetGuid(locKeyOwner); // ValueOf(rd, "KeyCorpREOwnerName");
                        var key = GetString(rd, locFullName); // ValueOf<string>(rd, "CREOwnerFullName");
                        _names.Add(key, value);
                    }
                    rd.Close();
                }
                _log.Debug("Retrieved {0:n0} unique names", _names.Count);
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Could not retrieve unique names");
            }
        }

        public Guid GetKey(OwnerName arg)
        {
            var ret = Guid.Empty;
            var name = arg.FullName;

            if (_names.ContainsKey(name))
                ret = _names[name];
            else
            {
                if (_unmatachedNames.Exists(x => string.Compare(x.FullName, name, true) == 0))
                    ret = _unmatachedNames.FirstOrDefault(x => string.Compare(x.FullName, name, true) == 0).Key;
                else
                {
                    arg.Key = Guid.NewGuid();
                    _unmatachedNames.Add(arg);
                    ret = arg.Key;
                }
            }
            return ret;
        }

        public void Update()
        {
            if (_unmatachedNames.Count > 0)
            {
                using (var namesaver = new OwnerNameSaver(_log, _unmatachedNames))
                {
                    if (namesaver.Save())
                    {
                        var tmp = namesaver.FailedRecords.ToList();
                        if (tmp.Count > 0)  // If there are any errors, make the error message for all the failed records and display only once.
                        {
                            var sb = new StringBuilder();
                            sb.AppendFormat("{0} record(s) failed to insert\n", tmp.Count);
                            tmp.ForEach(x =>
                            sb.AppendFormat("{0} {1} {2}",
                                _unmatachedNames[x.Item1].FullName,
                                _unmatachedNames[x.Item1].Key.ToString(),
                                x.Item2
                            ));
                        }

                        // now add only the records that didn't fail. so 
                        // loop through, if the index is not in the failed records then add otherwise discard.
                        var markedIndices = tmp.Select(x => x.Item1);
                        var addedRecords = 0;
                        for (int i = 0; i < _unmatachedNames.Count; i++)
                        {
                            if (!markedIndices.Contains(i))
                            {
                                _names.Add(_unmatachedNames[i].FullName, _unmatachedNames[i].Key);
                                addedRecords++;
                            }
                        }
                        _log.Info("{0} records were inserted", addedRecords);
                        _unmatachedNames.Clear();
#if SAVE
                } 
#endif
                    }
                }

            }
        }

        public int PendingRecords
        {
            get { return _unmatachedNames.Count; }
        }
    }
}
