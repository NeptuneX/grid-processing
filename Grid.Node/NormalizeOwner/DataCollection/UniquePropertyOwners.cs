﻿using Grid.Common;
using Grid.Node.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node.NormalizeOwner.Collectors
{
    public class UniquePropertyOwners : DataCollection
    {
        readonly PropertyOwnerType[] K_AllPropertyOwnerTypes = new PropertyOwnerType[]
                {PropertyOwnerType.FirstOwner,
                PropertyOwnerType.SecondOwner,
                PropertyOwnerType.CareTaker };

        Dictionary<string, PropertyOwnerRelation> _propertyOwners;
        List<PropertyOwnerRelation> _missingOwners;
        readonly string _countyfip;

        Func<long, PropertyOwnerType, string> MakeKey = (pkey, tcode) => string.Format("{0}|{1}", pkey, (int)tcode);
        // 3463874|0

        internal UniquePropertyOwners(IJournal log, string countyfip) : base(log)
        {
            _propertyOwners = new Dictionary<string, PropertyOwnerRelation>();
            _missingOwners = new List<PropertyOwnerRelation>();
            _countyfip = countyfip;

            try
            {
                using (var rd = GetReader(TSQL.GET_UNIQUE_OWNERS, new SqlArguments { { "@countyfip", _countyfip } }))
                {
                    int locKey = rd.GetOrdinal("KeyCorpREPropertyOwner");
                    int locKeyCorpREOwnerName = rd.GetOrdinal("KeyCorpREOwnerName");
                    int locKeyCorpREProperty = rd.GetOrdinal("KeyCorpREProperty");
                    int locKeyCREOwnerType = rd.GetOrdinal("KeyCREOwnerType");
                    while (rd.Read())
                    {
                        var rec = new PropertyOwnerRelation(
                            rd.GetGuid(locKey),
                            rd.GetInt32(locKeyCorpREProperty), //ValueOf<long>(rd, "KeyCorpREProperty"),
                            rd.GetGuid(locKeyCorpREOwnerName),  //ValueOf(rd, "KeyCorpREOwnerName")
                            (PropertyOwnerType)rd.GetByte(locKeyCREOwnerType) // ValueOf<int>(rd, "KeyCREOwnerType")
                            );
                        _propertyOwners.Add(MakeKey(rec.propertyKey, rec.type), rec);
                    }
                    rd.Close();
                }
                _log.Debug("Retrieved {0:n0} property owner relationships", _propertyOwners.Count);
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Could not get the property owner information");
            }
        }

        public bool Exist(long propertyKey, Guid ownerKey, PropertyOwnerType type)
        {
            var ret = false;
            var keyOwner1 = MakeKey(propertyKey, type);

            // SZ: Check the owner of type exist?
            if (_propertyOwners.ContainsKey(keyOwner1))
                ret = true;  // Retain the existing key, owner will not be changed.
            else
            {
                // SZ: Now that the owner of that type is not present. check if the owner is in other types 
                var otherTypes = K_AllPropertyOwnerTypes.Where(x => x != type).ToList();
                foreach (var foo in otherTypes)
                {
                    var keyOther = MakeKey(propertyKey, foo);
                    if (_propertyOwners.ContainsKey(keyOther))
                    {
                        if (Guid.Equals(ownerKey, _propertyOwners[keyOther]))
                        {
                            ret = true;  // Yes, the owner is not of requested type but is already owner with another type. Dont process further
                            break;
                        }

                    }
                }
            }
            return ret;
        }

        public void Add(long propertyKey, Guid ownerKey, PropertyOwnerType type)
        {

            if (!_missingOwners.Any(x => x.propertyKey == propertyKey && x.type == type) &&
                !_missingOwners.Any(x => x.propertyKey == propertyKey && Guid.Equals(x.ownerKey, ownerKey))
               )//To avoid duplicates.
                _missingOwners.Add(new PropertyOwnerRelation(propertyKey, ownerKey, type));
        }

        public void Update()
        {
            if (_missingOwners.Count > 0)
            {
                using (var ptyOwnersOperation = new PropertyOwnerSaver(_log, _missingOwners))
                {

                    if (ptyOwnersOperation.Save())
                    {

                        var tmp = ptyOwnersOperation.FailedRecords.ToList();
                        if (tmp.Count > 0) // If there are any errors, make the error message for all the failed records and display only once.
                        {
                            var sb = new StringBuilder();
                            sb.AppendFormat("{0} record(s) failed for the property owner relationship\n", tmp.Count());
                            tmp.ForEach(x => sb.AppendFormat("property: {0} owner type: {1} Owner {2}",
                                    _missingOwners[x.Item1].propertyKey,
                                    Enum.GetName(typeof(PropertyOwnerType), _missingOwners[x.Item1].type),
                                    _missingOwners[x.Item1].ownerKey.ToString()
                                    ));
                            _log.Error(sb.ToString());
                        }

                        // now add only the records that didn't fail. so 
                        // loop through, if the index is not in the failed records then add otherwise discard.
                        var markedIndices = tmp.Select(x => x.Item1);
                        var addedRecords = 0;
                        for (int i = 0; i < _missingOwners.Count; i++)
                        {
                            if (!markedIndices.Contains(i))
                            {
                                _propertyOwners.Add(
                                    MakeKey(_missingOwners[i].propertyKey, _missingOwners[i].type),
                                    _missingOwners[i]);
                                addedRecords++;
                            }
                        }
                        _missingOwners.Clear();
                        _log.Info("{0} records were inserted", addedRecords);
                    }

                }
            }
        }

        public int PendingRecords
        {
            get { return _missingOwners.Count; }
        }

        public void Dispose()
        {
            _propertyOwners.Clear();
            _missingOwners.Clear();

            base.Dispose();
        }
    }
}
