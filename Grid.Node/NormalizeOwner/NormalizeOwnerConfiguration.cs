﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node
{
    class NormalizeOwnerConfiguration : Common.ConfigurationReader<NormalizeOwnerConfiguration>
    {
        protected override void Init()
        {
            ConnectionString = Read<string>("InternalSystemsConnectString");
            BatchSize = Read<int>("BatchSize");
        }

        public string ConnectionString { get; private set; }
        public int BatchSize { get; private set; }
    }
}
