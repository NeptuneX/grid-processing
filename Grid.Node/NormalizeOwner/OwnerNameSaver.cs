﻿using Grid.Common;
using Grid.Node.Common;
using SNL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node.NormalizeOwner
{
    public class OwnerNameSaver : ObjectSaver<CREOwnerNamesClass>
    {
        IList<OwnerName> _list;
        IList<Guid> _failed;
        public OwnerNameSaver(IJournal log, List<OwnerName> arg) : base(log)
        {
            _list = arg.AsReadOnly();
            _failed = new List<Guid>();
        }

        protected override bool GetDataAt(int index, ref Instance inst)
        {
            var ret = index < _list.Count;
            var it = _list[index];

            inst.PutItemValue("CREOwnerFullName", it.FullName);
            if (!string.IsNullOrEmpty(it.FirstName)) inst.PutItemValue("CREOwnerFirstName", it.FirstName);
            if (!string.IsNullOrEmpty(it.MI)) inst.PutItemValue("CREOwnerMiddleName", it.MI);
            if (!string.IsNullOrEmpty(it.LastName)) inst.PutItemValue("CREOwnerLastName", it.LastName);
            if (!string.IsNullOrEmpty(it.Suffix)) inst.PutItemValue("CREOwnerSuffix", it.Suffix);

            return ret;
        }
        protected override string GetKeyAt(int index)
        {
            return _list[index].Key.ToString();
        }
        protected override long GetTotalRecords()
        {
            return _list.Count;
        }
    }
}
