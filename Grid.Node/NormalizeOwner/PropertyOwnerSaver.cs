﻿using Grid.Common;
using Grid.Node.Common;
using SNL.Data;
using System.Collections.Generic;

using DBG = System.Diagnostics.Debug;

namespace Grid.Node.NormalizeOwner
{
    public class PropertyOwnerSaver : ObjectSaver<CREPropertyOwnersClass>
    {
        IList<PropertyOwnerRelation> _records;


        public PropertyOwnerSaver(IJournal log, List<PropertyOwnerRelation> arg) : base(log)
        {
            _records = arg.AsReadOnly();
        }
        protected override bool GetDataAt(int index, ref Instance inst)
        {
            DBG.Assert(index < _records.Count);

            var it = _records[index];

            inst.PutItemValue("KeyCorpREOwnerName", it.ownerKey.ToString());
            inst.PutItemValue("KeyCorpREProperty", it.propertyKey);
            inst.PutItemValue("KeyCREOwnerType", (int)it.type);

            return true;
        }

        protected override string GetKeyAt(int index)
        {
            return _records[index].key.ToString();
        }

        protected override long GetTotalRecords()
        {
            return _records.Count;
        }
    }
}
