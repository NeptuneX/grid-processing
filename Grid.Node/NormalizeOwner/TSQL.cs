﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node.NormalizeOwner
{
    public sealed class TSQL
    {
        public const string GET_CURRENT_OWNER_INFO = @"With OldStructure AS (SELECT ROW_NUMBER() OVER (Order by KeyCorpREProperty) as Rowid, KeyCorpREProperty, CREPropertyOwnerFirstLastName, CREPropertyOwnerFirstFirstName, CREPropertyOwnerFirstMI, CREPropertyOwnerFirstSuffix, 
                      CREPropertyOwnerFirst, CREPropertyOwnerSecondLastName, CREPropertyOwnerSecondFrstName, CREPropertyOwnerSecondMI, CREPropertyOwnerSecondSuffix, 
                      CREPropertyOwnerSecond, CREPropertyOwnerCaretaker, CREPropertyZip
                            FROM         ObjectViews..CREProperty
                                Where CountyFIPS  =@zip 
                            )
                        Select rowid, KeyCorpREProperty, CREPropertyOwnerFirstLastName, CREPropertyOwnerFirstFirstName, CREPropertyOwnerFirstMI, CREPropertyOwnerFirstSuffix, 
                                              CREPropertyOwnerFirst, CREPropertyOwnerSecondLastName, CREPropertyOwnerSecondFrstName, CREPropertyOwnerSecondMI, CREPropertyOwnerSecondSuffix, 
                                              CREPropertyOwnerSecond, CREPropertyOwnerCaretaker, CREPropertyZip
					                          from OldStructure";

        public const string GET_BACKFILL_COUNT = @"SELECT count(*) FROM Calcs..CorpREProperty where CREPropertyZip =@zip";

        public const string GET_UNIQUE_OWNERS = @"Select distinct O.KeyCorpREPropertyOwner, O.KeyCorpREProperty, O.KeyCorpREOwnerName, O.KeyCREOwnerType 
                                                    from ObjectViews..CREPropertyOwner O inner join 
                                                    ObjectViews..CREProperty P on O.KeyCorpREProperty = P.KeyCorpREProperty
                                                    Where P.CountyFIPS = @countyfip
                                                    Order by KeyCorpREProperty";


        internal static readonly string GET_CODES_AND_COUNT = @"select CountyFIPS, count(*) as ZipCount from ObjectViews..CREProperty group by CountyFIPS  order by ZipCount";
        internal static readonly string GET_OWNER_TYPES = @"Select * from Lookup..CREOwnerType";
        internal static readonly string GET_UNIQUE_NAMES = @"Select distinct CREOwnerFullName, KeyCorpREOwnerName  from ObjectViews..CREOwnerName Order by CREOwnerFullName";
    }
}
