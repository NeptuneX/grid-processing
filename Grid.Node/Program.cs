﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Threading;
using Fclp;
using Grid.Common;
using Grid.Common.Process;
using Grid.Communicator;
using Grid.Communicator.Common;
using static System.Console;

namespace Grid.Node
{
    class Program
    {
        static void Main(string[] args)
        {
            var  journal = (IJournal)null;
            var signal = (ManualResetEventSlim) null;
            var threads = new List<Thread>(3);

            try
            {
                journal = new NLogJournal("node");

                journal.Debug($"reading configuration....");
                var config  = ParseArguments(args);

                journal.Debug($"Node [{config.Item1.name}] is initializing....");
                signal = new ManualResetEventSlim(false);

                journal.Debug("Setting up internal channels for user code...");

                var inputChannel = new Channel(1024);
                var outputChannel = new Channel(4096);
                var inBuffer = new DataBuffer<object>(10000, false, false);
                var outBuffer = new DataBuffer<object>(10000, false, false);


                var policy = ReadPolicy();

                journal.Debug("spawning secondary threads...");

                if (Utility.IsPortOpen(config.Item1.host.Port))
                    throw new Exception($"Port {config.Item1.host.Port} is not open");

                threads.Add( CreateCommunicator(config.Item1, journal, signal, policy, inBuffer, outBuffer) );
                threads.Add( CreateExecuter(config.Item2, signal, inputChannel, outputChannel));
                threads.Add( CreateCache(config.Item1.name, journal, signal, inputChannel, outputChannel, inBuffer, outBuffer));

                var input = new InputProcessor();

                threads.ForEach(x => x.Start());

                while (!input.ShouldQuit)
                {
                    Thread.Sleep(450);
                    Thread.Yield();
                }

                signal.Set();

                foreach (var th in threads)
                {
                    var ret = th.Join(TimeSpan.FromSeconds(40));
                    if (!ret)
                    {
                        journal.Debug($"thread {th.Name} failed to end, terminating ...");
                        th.Abort();
                    }
                }

                journal.Info($"shutdown of {config.Item1.name} is complete");

                NLog.LogManager.Flush();
                NLog.LogManager.Shutdown();
            }
            catch (Exception ex)
            {
                if(journal!=null)
                    journal.Error(ex, "Error in executing node");
            }
        }

        private static CommunicationPolicy ReadPolicy()
        {
            return new CommunicationPolicy(
                        messageTimeout: TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["messageTimeout"])) ,
                        idletime: TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["idletime"])),
                        maxretries: int.Parse(ConfigurationManager.AppSettings["maxretries"]),
                        maxobjects: int.Parse(ConfigurationManager.AppSettings["maxobjects"])
                        );
        }

        private static void PrintSyntax()
        {
            var message = $@"
                      Syntax: node /n:<name> [/ip:<host ip:port>] [/f:<use code dll>] [/d:<targets ip addresses>]
                     =========================================================================
                     n name name of the node
                     i ip ip address with port(1000 - 65000), if not provided OR if * is provided, program will use 127.0.0.1:1000
                     f file user dll,classname in which INodeTask is implemented
                     d destination destination space separated list of  targets ip1:port1 ip2:port2 ip3:port3
                     ======================================================================= ";

            WriteLine(message);
        }

        /// <summary>
        /// Parses the command line
        /// =========================================================================
        /// n name      name of the node
        /// i ip        ip address with port(1000 - 65000), if not provided, program will use 127.0.0.1:1000 
        /// f file      user dll,classname in which INodeTask is implemented
        /// d destination destination space separated list of  targets ip1:port1 ip2:port2 ip3:port3
        /// =======================================================================
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Tuple<CommunicatorConfig, UserCodeConfig> ParseArguments(string[] args)
        {
            Contract.Requires(args.Length > 0);

            Tuple<CommunicatorConfig, UserCodeConfig> ret = null;
            try
            {
                var name = string.Empty;
                var dll = string.Empty;
                var code = string.Empty;
                var destinations = new List<IPEndPoint>();
                var host = (IPEndPoint) null;
                var targets = new List<string>();

                var p = new FluentCommandLineParser();
                p.Setup<string>('n', "name").Callback(x => name = x);
                p.Setup<string>("ip").Callback(y => host = Utility.ToIPEndPoint(y)).SetDefault(Utility.LocalHost.ToString());
                p.Setup<string>('f', "file").Required().Callback(x =>
                {
                    var tmp = x.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    dll = tmp[0].Trim();
                    if (tmp.Length >1)
                        code = tmp[1].Trim();
                });
                p.Setup<List<string>>('d', "destination").Callback(items => targets = items); 
                p.Parse(args);

                targets.ForEach(x => destinations.Add(Utility.ToIPEndPoint(x)));

                if (string.IsNullOrEmpty(dll))
                    throw new Exception("No file specified for user code with NodeTask implementation");

                if (string.IsNullOrEmpty(name))
                    name = code;

                var commConfig =  new CommunicatorConfig(name, host, destinations);
                var ucConfig = new UserCodeConfig(dll, code);
                ret = new Tuple<CommunicatorConfig, UserCodeConfig>(commConfig, ucConfig);
            }
            catch (Exception ex)
            {
                WriteLine(ex.ToString());
                PrintSyntax();
            }
            return ret;
        }

        static Thread CreateCommunicator(CommunicatorConfig config, IJournal journal, ManualResetEventSlim signal, CommunicationPolicy policy, IObjectBuffer input, IObjectBuffer output)
        {
            var thread = new Thread(() =>
            {
                try
                {
                    using (var communicator = new Communicator.Communicator(journal, signal, policy, config.host, input, output))
                    {
                        foreach (var it in config.targets)
                            communicator.AddSink(it);

                        communicator.Run();
                    }
                }
                catch (Exception ex)
                {
                    journal.Error(ex, "error in communication thread");
                }

            });

            thread.Name = "Communicator";
            //thread.IsBackground = true;
           

            return thread;
        }

        static Thread CreateExecuter(UserCodeConfig config, ManualResetEventSlim signal, IChannel reader, IChannel writer)
        {
            var thread = new Thread(() =>
            {
                IJournal journal=null;

                try
                {
                    journal =  new NLogJournal("user");
                    var code = new UserCode(config.assembly, config.classname, signal, reader, writer);
                    code.Run();
                }
                catch (Exception ex)
                {
                    if(journal!=null)
                        journal.Error(ex, "Terminating user code thread");
                }
            });

            thread.Name = "Executer";
            thread.IsBackground = true;
            
            return thread;
        }

        static Thread CreateCache(string name, IJournal journal, ManualResetEventSlim signal, IChannel reader, IChannel writer, IObjectBuffer input, IObjectBuffer output)
        {
            var thread = new Thread(() =>
            {
                try
                {
                    using (var cache = new CacheManager(journal, name, signal, input, output, reader, writer))
                    {
                        cache.Run();
                    }
                }
                catch (Exception ex)
                {
                    journal.Error(ex, "Terminating user code thread");
                }
            });

            thread.Name = "Cache";
            thread.IsBackground = true;
            
            return thread;
        }
    }
}

