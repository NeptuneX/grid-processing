﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node
{
    public class TableInfo
    {
        public string TableName { get; set; }
        public string KeyName { get; set; }
        public Dictionary<string, int> TableColumns { get; set; }
        // IncludeKeyColumn will include KeyCorpRE<OID> column in the Audit table.
        public bool IncludeKeyColumn { get; set; }
        // IsKeyNull will either assign random guid for KeyCorpRE<OID> or keep it blank when false.
        public bool IsKeyNull { get; set; }
        // When IncludeKeyColumn is true, KeyTypeCode determines if datatype of the KeyColumn. For Properties, the KeyCorpREProperty is an integer.
        // For KeyCorpREMortgage and KeyCorpRETransaction, the type is GUID. There is no GUID type in TypeCode. So, Object typecode is treated as Guid.
        public TypeCode KeyTypeCode { get; set; }
    }
}
