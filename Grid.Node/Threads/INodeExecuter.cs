﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grid.Node.Threads
{
    interface INodeExecuter
    {
        int Id { get; }
        bool IsRunning { get; }

        void Start(NodeContext context);
        void Kill();
    }
}
