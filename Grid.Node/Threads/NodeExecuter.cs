﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Grid.Common;
using Grid.Common.Process;

namespace Grid.Node.Threads
{


    internal class NodeExecuter: INodeExecuter
    {
        readonly Thread _thread;
        readonly Tuple<Type, Object[]> _typeDetail;


        private NodeExecuter()
        {
            _thread = new Thread((arg) => Run(
                    (arg as Tuple<NodeContext, Type, Object[]>).Item1,
                    (arg as Tuple<NodeContext, Type, Object[]>).Item2,
                    (arg as Tuple<NodeContext, Type, Object[]>).Item3
                    ));

            _thread.IsBackground = true;
            _thread.Priority = ThreadPriority.Normal;
        }
        internal NodeExecuter(string path, params object[] args): this()
        {
            Contract.Requires(File.Exists(path));
            Contract.Ensures(_thread!=null);

            var assembly = Assembly.LoadFile(path);
            Contract.Assert(assembly != null);

            var type = assembly.GetTypes().Where(x => x is INodeTask).FirstOrDefault();
            Contract.Assert(IsNodeTask(type));

            _typeDetail = new Tuple<Type, object[]>(type, args);
            _thread.Name = $"[{type.Name}]";
        }
        internal NodeExecuter(Type type, params object[] args) : this()
        {
            Contract.Assert(IsNodeTask(type));
            Contract.Ensures(_thread != null);

            _typeDetail = new Tuple<Type, object[]>(type, args);
            _thread.Name = $"[{type.Name}]";
        }

        [Pure] private bool IsNodeTask(Type type)
        {
            return type!=null && type.GetInterfaces().Where(x => x == typeof(INodeTask)).ToList().Count==1;
        }

        private void Run(NodeContext ctx, Type type, params object[] args)
        {
            var process = (INodeTask) null;
            var exit = false;

            while (!exit)
            {
                switch (ctx.State)
                {
                    case ExecutionState.Load:
                        Sandbox.Try(out process,
                                () => Activator.CreateInstance(type,
                                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                                    null,
                                    args,
                                    CultureInfo.CurrentCulture) as INodeTask,
                              (ex) => ctx.Fault(ex));
                        break;

                    case ExecutionState.Initialize:
                        Sandbox.Try(() => process.Init(ctx), (ex) => ctx.Fault(ex));
                        break;

                    case ExecutionState.Run:
                        Sandbox.Try(() => process.Run(ctx), (ex) => ctx.Fault(ex));
                        break;

                    case ExecutionState.Destroy:
                        Sandbox.Try(() => process.Destroy(ctx), (ex) => ctx.Fault(ex));
                        break;

                    case ExecutionState.Terminate:
                        exit = true;
                        break;
                }
                ctx.NextStage();
            }//End of node life cycle

            ctx.Journal.Info($"Finished Execution of {_thread.Name}"); 
        }

        void INodeExecuter.Start(NodeContext context)
        {
            Contract.Requires(context != null);
            Contract.Requires(!_thread.IsAlive);
            Contract.Ensures(_thread.IsAlive);

            _thread.Start(new Tuple<NodeContext, Type, Object[]>(context, _typeDetail.Item1, _typeDetail.Item2));
        }
        bool INodeExecuter.IsRunning => _thread.IsAlive;

        int INodeExecuter.Id => _thread.ManagedThreadId;

        void INodeExecuter.Kill() => _thread.Abort();
    }
}
