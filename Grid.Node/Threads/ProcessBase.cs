﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading;

namespace Grid.Node.Threads
{
    internal class ExecutionContextBuilder
    {
        struct PipeUsers
        {
            private readonly int Source;
            private readonly int Target;

            PipeUsers(int source, int target)
            {
                Source = source;
                Target = target;
            }
        }

        readonly List<PipeUsers> _users;

        internal ExecutionContextBuilder()
        {
            _users = new List<PipeUsers>();
        }

    }
}
