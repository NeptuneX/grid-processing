﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyOS;

namespace Grid.Node
{
    class Consumer : TinyProcessBase
    {
        public override void Run()
        {
            while (Context.Continue)
            {
                if (Context.HasInput)
                {
                    var x = Context.Read<string>();

                    Context.Write($"ready by consumer: {x}");
                }
            }
        }
    }
}
