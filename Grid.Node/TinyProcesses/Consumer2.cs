﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using TinyOS;

namespace Grid.Node
{
    class Consumer2 : TinyProcessBase
    {
        public override void Run()
        {
            var ctx = Context;

            ctx.Write("Consumer II has started");

            while (ctx.Continue)
            {
                var batchSize = 5;
                var list = new List<object>(batchSize);

                using (var cache = new GridHolder(new InMemoryProvider.InMemoryProvider(Context.Journal), Context.Journal))
                {
                    while (!cache.Inbox.IsEmpty || batchSize-- == 0)
                    {
                        var tmp = cache.Inbox.Pop();
                        list.Add(tmp);
                    }
                }

                foreach (var it in list)
                    Context.Write(it);
            }
        }
    }
}
