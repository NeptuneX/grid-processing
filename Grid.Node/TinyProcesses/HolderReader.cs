﻿using System;
using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using TinyOS;

namespace Grid.Node
{

    class HolderReader<M> : TinyProcessBase where M : IHolderStorageProvider
    {
        GridHolder _holder;


        protected override void Init()
        {
            var provider = (IHolderStorageProvider)Activator.CreateInstance(typeof(M), Context.Journal);
            _holder = new GridHolder(provider, Context.Journal);
        }

        public override void Run()
        {
            while (true)
            {
                while(!_holder.Inbox.IsEmpty)
                {
                    var tmp = _holder.Inbox.Pop();
                    Context.Write(tmp);
                }
            }
        }
    }
}
