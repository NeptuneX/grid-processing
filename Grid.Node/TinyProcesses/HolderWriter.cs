﻿using System;
using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using TinyOS;

namespace Grid.Node
{
    public class HolderWriter<T, M> : TinyProcessBase where M : IHolderStorageProvider
    {

        GridHolder _holder;

        protected override void Init()
        {
            _holder = new GridHolder((IHolderStorageProvider)Activator.CreateInstance(typeof(M), Context.Journal), Context.Journal);
        }

        public override void Run()
        {
            while (Context.Continue)
            {
                var items = 0;
                while (Context.HasInput)
                {
                    var tmp = Context.Read<T>();
                    _holder.Inbox.Push(tmp);
                    items++;
                }
                if(items>0)
                    Context.Write($"stored {items} items");
            }

            _holder.Dispose();
        }
    }
}