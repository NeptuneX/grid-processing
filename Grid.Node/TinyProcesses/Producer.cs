﻿using System;
using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using TinyOS;
using InMemoryProvider;

namespace Grid.Node
{
    public class Producer : TinyProcessBase
    {
        public override void Run()
        {
            var ctx = Context;

            ctx.Write("Producer has started");
            while (ctx.Continue)
            {
                var work = 10;
                while (work-- > 0)
                {
                    using (var cache = new GridHolder(new InMemoryProvider.InMemoryProvider(Context.Journal), Context.Journal))
                    {
                        for (var i = 0; i < 20; i++)
                        {
                            var txt = $"Batch {work} : {i} Hello World";
                            cache.Inbox.Push(txt);
                            ctx.Write(txt);
                        }
                    }
                    ctx.Write($"Wrote batch {work}, now resting");
                    ctx.Sleep(TimeSpan.FromMilliseconds(100));
                }
                ctx.Shutdown();
                break;
            }

            ctx.Write("Producer is done");
        }
    }
}
