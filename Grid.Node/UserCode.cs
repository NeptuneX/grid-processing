﻿using System;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using Grid.Common;
using Grid.Common.Process;
using Grid.Node.Threads;

namespace Grid.Node
{
    class UserCode
    {
        readonly string _assembly;
        readonly string _class;
        //readonly string _name;

        readonly IJournal _journal;
        readonly ManualResetEventSlim _signal;


        readonly IChannelReader _reader;
        readonly IChannelWriter _writer;



        public UserCode(string assembly, string task, ManualResetEventSlim evt, IChannelReader rdr, IChannelWriter wtr)
        {
            Contract.Requires(evt != null);
            Contract.Requires(rdr != null);
            Contract.Requires(wtr != null);

            _journal = new NLogJournal("user");
            _journal.Debug("Initializing secondary thread for user code ...");

            //_name = config.name;
            _assembly = assembly;
            _class = task;

            _signal = evt;
            _reader = rdr;
            _writer = wtr;
            try
            {
                _assembly = Path.GetFullPath(_assembly);
                _journal.Info($"Task to load {_assembly}.{_class}");


                if (!File.Exists(_assembly))
                    throw new FileNotFoundException($"Could not find file {_assembly}");
            }
            catch (Exception ex)
            {
                _journal.Error(ex, $"Failed to load the specified user task");
            }
        }

        
        public void Run()
        {
            var usercode = (INodeTask) null;
            var context = new NodeContext(_reader, _writer, _journal);

            var lifeCycle = true;
            while (lifeCycle)
            {
                var ret = false;
                switch (context.CurrentState)
                {
                    case ExecutionState.Load:
                        ret = Sandbox.Try(out usercode, () => LoadUserCode(), context.Fault);
                        if (ret && usercode != null)
                        {
                            //usercode = library.Item3;
                            if (usercode == null)
                            {
                                context.Fault(new Exception($"could not load user code {_assembly}.{_class}. Terminating the executor..."));
                                lifeCycle = false;
                            }
                            else
                                _journal.Info($"User code loaded successfully. Execution starts");
                        }
                        else
                            lifeCycle = false;
                        break;
                    case ExecutionState.Initialize:
                        ret = Sandbox.Try(() => usercode.Init(context), context.Fault);
                        if (ret)
                            _journal.Info("Initialization stage completed successfully");
                        else
                            _journal.Info("Initialization of user code failed. Terminating the executor...");
                        break;
                    case ExecutionState.Run:
                        ret = Sandbox.Try(() => usercode.Run(context), context.Fault);
                        break;
                    case ExecutionState.Destroy:
                        ret = Sandbox.Try(() => usercode.Destroy(context), context.Fault);
                        if (ret)
                            _journal.Info($"user task destroyed successfully");
                        break;
                    case ExecutionState.Terminate:
                        //AppDomain.Unload(library.Item1);
                        //library = null;
                        usercode = null;
                        lifeCycle = false;
                        break;
                }
                if (_signal.IsSet)
                    context.Exit();
                context.Continue();
            }

            context.Journal.Info("User code execution finished...");
        }
        private INodeTask LoadUserCode()
        {
            INodeTask ret = null;

                var type = typeof(AssemblyLoader);
                var loader = (AssemblyLoader) AppDomain.CurrentDomain.CreateInstanceAndUnwrap(type.Assembly.FullName, type.FullName);
                var assembly = loader.GetAssembly(_assembly);
                var filename =Path.GetFileNameWithoutExtension(_assembly);

                foreach (var it in assembly.GetTypes())
                {
                    if (string.Compare(it.Name, _class, true) == 0)
                    {
                        ret = assembly.CreateInstance(it.FullName, true,
                            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance,
                            null, null, CultureInfo.CurrentCulture,
                            null) as INodeTask;
                        break;
                    }
                }

            return ret;
        }
    }
}
