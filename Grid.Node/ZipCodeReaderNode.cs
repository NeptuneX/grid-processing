﻿using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using Grid.Node.NormalizeOwner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyOS;

namespace Grid.Node
{
    public class ZipCodeReaderNode : TinyProcessBase
    {

        private ZipCodeCollector _zipCodes;
        protected override void Init()
        {
            base.Init();
            _zipCodes = new ZipCodeCollector(Context.Journal);
        }
        public override void Run()
        {

            var ctx = Context;

            ctx.Write("Zip Code Reader Node has started");
            while (ctx.Continue)
            {
                //var work = 10;
                //while (work-- > 0)
                //{
                using (var cache = new GridHolder(new InMemoryProvider.InMemoryProvider(Context.Journal), Context.Journal))
                {

                    using (_zipCodes)
                    {
                        foreach (var zipcode in _zipCodes)
                        {
                            Console.WriteLine($"Pushing {zipcode.Key}");
                            cache.Inbox.Push(zipcode);
                            //ctx.Sleep(TimeSpan.FromMilliseconds(10));

                            //ctx.Write(zipcode);
                        }
                    }

                    /*for (var i = 0; i < 20; i++)
                    {
                        var txt = $"Batch {work} : {i} Hello World";
                        cache.Inbox.Push(txt);
                        ctx.Write(txt);
                    }*/
                }
                ctx.Write($"Wrote batch {1}, now resting");
                //ctx.Sleep(TimeSpan.FromMilliseconds(100));
                //}
                //ctx.Shutdown();
                break;
            }

            ctx.Write("Reader Node is done");
        }
    }
}
