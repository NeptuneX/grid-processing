Distributed Computing
=====================

> A distributed system is a model in which components located on networked computers communicate and coordinate their actions by passing messages

The objective is to run each grid component separately in a way that they can send and receive data irrespective of any other component running or not or how many of its own instances running.

Grid Object
-----------

-   An object is any serializable .NET object that is the data that flows in the grid one points to other.
-   At any given instance, any object can be at one and only one node only.
-   An object must not depend on other objects or any particular location or specific conditions

Node Task
---------

-   A node task or a task is the smallest processing unit that can produce, process, or push the objects.
-   It has no knowledge of the point of origin of the object it receives nor can it presume where it would be sent afterwards, if it pushes it
-   A task can be instantiated and destroyed as many times as needed, can execute at any point on the grid and has no knowledge of other instances of itself, neither can it query.
-   An assembly can contain a single task or multiple tasks or a single task may span multiple assemblies 

Job
---

-   A job is a combination of various tasks and defines the object flow between them
-   The objects and tasks of one job are not sharable with other jobs.
-   There can be multiple jobs executing in the grid at the same time and can neither query or share any information with each other.
-   Each task of the job can be have any number of allowed instances

Grid Computing
--------------

-   Implements pipeline parallelism, a pipeline with its stages is defined by a Job that consists of one or more node tasks
-   A single stage Job whose result is folded back onto initial stage makes it grid computing in strict sense.

Components
----------

-   Communicator
-   Task Executer
-   Cache Manager

### Communicator

-   Implements a transactional protocol using sockets to send and receive data (hidden from the users)
-   Any node that sends data is a SOURCE, any node that receives it is a SINK.
-   Is able to send data to multiple receivers sinks and can receive data from multiple sources

### Cache Manager

-   Uses local data storage to store the objects received or to be sent.
-   Makes communication of Fast producer – slow Consumer possible
-   Makes possible to continue the process where it left off / crashed

### Executer

-   Loads the Node Task and execute them.
-   The task can produce data, consume the incoming data or consume and produce data for next stage.
-   A user task crash is isolated and does not affect the cache manager, communicator or the whole node.

Data Flow (Source)
------------------

1.  On Startup, the communicator, Cache manager and executer starts running in separate threads.
2.  Executor loads the node task and starts executing.
3.  The node task produces some data and puts it on the outgoing channel (from executer to cache manager).
4.  The channel is constantly monitored by the Cache manager.
5.  If there is no more data to send, cache manager reads the data produced by user task and puts it on the outgoing channel (from cache manager to communicator). Otherwise the data is put in cache.
6.  When data is available on outgoing channel (from cache manager to communicator), the communicator sends the data out to the next stage (if there is any)
7.  If the node is shutdown, data from channels from user task and communicator is flushed onto cache.

Data Flow (Sink)
----------------

1.  On Startup, the communicator, Cache manager and executer starts running in separate threads.
2.  When the communicator receives the data it puts onto incoming channel (from communicator to cache manager)
3.  When cache manager has data from communicator it either puts data on channel from itself to executer if there is no pending data for processing or puts it into a cache file
4.  The data is fetched by the user task and is consumed.
5.  If the node is shutdown, data from channels from user task and communicator is flushed onto cache.