﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grid.Common;

namespace UnitTests
{
    class EmptyJournal : IJournal
    {
        public void Debug(string msg, params object[] args)
        {
        }

        public void Debug(Exception ex, string msg = "", params object[] args)
        {
        }

        public void Error(string msg, params object[] args)
        {
        }

        public void Error(Exception ex, string msg = "", params object[] args)
        {
        }

        public void Info(string msg, params object[] args)
        {
        }

        public void Warn(string msg, params object[] args)
        {
        }

        public void Warn(Exception ex, string msg = "", params object[] args)
        {
        }
    }
}
