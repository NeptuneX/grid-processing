﻿using System;
using System.Collections.Generic;
using Grid.Holder;
using Grid.Holder.Providers.liteDb;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class HolderTests
    {
        GridHolder holder;

        [TestInitialize]
        public void Init()
        {
            holder = new GridHolder(new LiteDBProvider(new EmptyJournal()), new EmptyJournal());
            //holder.Initialize();
        }

        [TestCleanup]
        public void Cleanup()
        {
            holder.Dispose();
            holder = null;
        }

        [TestMethod]
        public void StringCanBeSaved()
        {
            var d1 = "hello World";

            holder.Inbox.Push(d1);


            var c1 = holder.Inbox.Pop() as string;

            Assert.AreEqual(c1, d1);
        }

        [TestMethod]
        public void DictionaryCanBeSaved()
        {
            var d1 = new Dictionary<string, object>{ {"a", 1234}, {"b", TimeSpan.FromDays(3)}, { "c", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 } }, {"d", DateTime.Now} };

            holder.Inbox.Push(d1);

            var g1 = holder.Inbox.Pop();

            var c1 = g1  as Dictionary<string, object>;

            Assert.IsInstanceOfType(c1, d1.GetType());
            Assert.AreEqual(d1.Keys.Count, c1.Keys.Count);
        }

        [TestMethod]
        public void TupleCanBeSaved()
        {
            var t1 = new Tuple<int, string, object>(234, "hello World", new KeyValuePair<int, string>(34, "John Doe"));

            holder.Inbox.Push(t1);


            var c1 = holder.Inbox.Pop() as Tuple<int, string, object>;

            Assert.IsInstanceOfType(c1, t1.GetType());

            Assert.AreEqual(c1.Item1, t1.Item1);
            Assert.AreEqual(c1.Item2, t1.Item2);
            Assert.AreEqual(c1.Item3.GetType().FullName, t1.Item3.GetType().FullName);

        }

        [TestMethod]
        public void ArrayCanBeSaved()
        {
            var t1 = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l' };

            holder.Inbox.Push(t1);


            var c1 = holder.Inbox.Pop() as char[];

            Assert.IsInstanceOfType(c1, t1.GetType());

            Assert.AreEqual(c1.Length, t1.Length);
            Assert.AreEqual(c1[0], t1[0]);
            Assert.AreEqual(c1[7], t1[7]);

        }

        [TestMethod]
        public void ListCanBeSaved()
        {
            var t1 = new List<int> { 1,2,3,4,5,6,7,8,9,0};

            foreach(var it in t1)
                holder.Inbox.Push(it);


            int idx = 0;
            while (!holder.Inbox.IsEmpty)
            {
                var c1 = (int)holder.Inbox.Pop();
                Assert.AreEqual(c1, t1[idx++]);
            }
        }


    }
}
